const { sendResponse } = require("../Utils");

exports.error404 = (req, res, next) => {
  return sendResponse(res, "error", 404, "Wrong API Request");
};

exports.errorHandler = (error, req, res, next) => {
  console.log("Error Name", error.message);
  console.log("Error Name", error);
  // Check for PostGres Error Handling Here

  const STRIPE_ERROR_TYPES = [
    "StripeCardError",
    "StripeRateLimitError",
    "StripeInvalidRequestError",
    "StripeAPIError",
    "StripeConnectionError",
    "StripeAuthenticationError",
  ];

  if (error?.type?.includes(STRIPE_ERROR_TYPES)) {
    console.log("------STRIPE ERROR------");
    return sendResponse(res, "error", error.statusCode, error.message);
  }

  const code = error.status || 500;
  return sendResponse(res, "error", code, error.message);
};

exports.welcome = (req, res, next) => {
  return sendResponse(res, "error", 200, "Welcome To Learnatric API");
};

exports.errorCatcher = (controller) => {
  return (req, res, next) => {
    Promise.resolve(controller(req, res, next).catch(next));
  };
};
