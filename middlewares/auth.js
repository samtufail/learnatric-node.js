const { Parent, Children } = require("../database");
const { JWT_Verify } = require("../Utils");

module.exports = {
  auth: async (req, res, next) => {
    try {
      let token = req.header("Authorization");
      if (!token) {
        return res.status(401).json({ message: "Please authenticate." });
      }
      token = token.replace("Bearer ", "");
      const decoded = JWT_Verify(token);
      if (!decoded) {
        return res.status(401).json({ message: "Please authenticate." });
      }
      let user = null;
      if (decoded.type == "parent") {
        user = await Parent.findOne({ where: { id: decoded.id } });
      }
      if (decoded.type == "children") {
        user = await Children.findOne({ where: { id: decoded.id } });
      }
      if (!user) {
        return res.status(401).json({ message: "Please authenticate." });
      }
      req.user = {
        ...user,
        type: decoded.type,
      };
      next();
    } catch (err) {
      return res.status(401).json({ message: "Please authenticate." });
    }
  },
};
