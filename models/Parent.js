module.exports = (connection, DataTypes) => {
  return connection.define("Parent", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    phoneNumber: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    hearedFrom: {
      type: DataTypes.STRING,
    },
    childs: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    stripeId: {
      type: DataTypes.STRING,
    },
    stripeSubsID: {
      type: DataTypes.STRING,
    },
    step: {
      type: DataTypes.STRING,
      defaultValue: 1,
    },
    noOfChildsProfileCompleted: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    noOfChildsCredsCompleted: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    is_free_signup: {
      type: DataTypes.BOOLEAN
    }, 
    did_cancel_subscription: {
      type: DataTypes.BOOLEAN, 
      defaultValue: false 
    }
  });
};
