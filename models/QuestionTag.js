module.exports = (connection, DataTypes) => {
  return connection.define("T_Q_Relationship", {
    t_q_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    tag_id: { type: DataTypes.INTEGER },
    question_id: { type: DataTypes.INTEGER },
    tag_rating: { type: DataTypes.INTEGER },
  });
};
