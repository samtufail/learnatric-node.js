const { toys } = require("../constants");
module.exports = (connection, DataTypes) => {
  return connection.define("Child", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    dob: {
      type: DataTypes.STRING,
    },
    gender: {
      type: DataTypes.STRING,
    },
    grade: {
      type: DataTypes.STRING,
    },
    school: {
      type: DataTypes.STRING,
    },
    teacherEmail: {
      type: DataTypes.STRING,
    },
    teacherName: {
      type: DataTypes.STRING,
    },
    schoolDistrict: {
      type: DataTypes.STRING,
    },
    canEmailTeacher: {
      type: DataTypes.BOOLEAN,
    },
    username: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING,
    },
    avatarSmall: {
      type: DataTypes.STRING,
    },
    avatarLarge: {
      type: DataTypes.STRING,
    },
    parentId: {
      type: DataTypes.INTEGER,
    },
    isFirstLogin: {
      type: DataTypes.BOOLEAN,
      default: true,
    },
    stepInLesson: {
      type: DataTypes.STRING,
    },
    questionIndex: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
    user_lesson_id: { type: DataTypes.INTEGER, defaultValue: null },
    lesson_id: { type: DataTypes.INTEGER, defaultValue: null },
    is_test_account: { type: DataTypes.BOOLEAN, defaultValue: false },
    is_active_subscription: { type: DataTypes.BOOLEAN, defaultValue: true },
    question_id: { type: DataTypes.INTEGER },
    assessment_complete: { type: DataTypes.BOOLEAN, defaultValue: false },
    assessment_index: { type: DataTypes.INTEGER, defaultValue: 1 },
    toys: { type: DataTypes.JSON, defaultValue: JSON.stringify(toys) },
    wobble: { type: DataTypes.INTEGER, defaultValue: 0 },
    trainNumber: { type: DataTypes.INTEGER, defaultValue: 1 },
  });
};
