module.exports = (connection, DataTypes) => {
  return connection.define("L_Q_Relationship", {
    l_q_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    lesson_id: { type: DataTypes.INTEGER},
    question_id: { type: DataTypes.INTEGER },
  }, {timestamps: false});
};