module.exports = (connection, DataTypes) => {
  return connection.define("Tag_Performance", {
    performance_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    child_id: { type: DataTypes.INTEGER },
    question_id: { type: DataTypes.INTEGER },
    lesson_id: { type: DataTypes.INTEGER },
    tag_id: { type: DataTypes.INTEGER },
    question_rating: { type: DataTypes.INTEGER },
    tag_rating_start: { type: DataTypes.FLOAT },
    is_correct: { type: DataTypes.BOOLEAN },
    rating_new: { type: DataTypes.FLOAT },
    is_we_do: { type: DataTypes.BOOLEAN, defaultValue: false },
  });
};
