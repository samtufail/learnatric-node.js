module.exports = (connection, DataTypes) => {
  return connection.define(
    "Tags",
    {
      tag_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      tags: { type: DataTypes.TEXT },
      domaine: { type: DataTypes.TEXT },
      domain_code: { type: DataTypes.TEXT },
      grade: { type: DataTypes.TEXT },
      tag_average_rating: { type: DataTypes.TEXT },
      tag_rating_start: { type: DataTypes.TEXT },
      tag_rating_max: { type: DataTypes.TEXT },
      standard_id: { type: DataTypes.TEXT },
      notes: { type: DataTypes.TEXT },
      standard_description: { type: DataTypes.TEXT },
    },
    { timestamps: false }
  );
};
