module.exports = (connection, DataTypes) => {
  return connection.define("User_Lessons", {
    user_lesson_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    child_id: { type: DataTypes.INTEGER },
    lesson_id: { type: DataTypes.INTEGER },
    instruction_completed: { type: DataTypes.BOOLEAN, defaultValue: false },
    activity_completed: { type: DataTypes.BOOLEAN, defaultValue: false },
    problem_set: { type: DataTypes.BOOLEAN, defaultValue: false },
    times_attempted: { type: DataTypes.INTEGER},
    // start_date_time: { type: DataTypes.TEXT, defaultValue: new Date() + "" },
    // end_date_time: { type: DataTypes.TEXT, defaultValue: null },
  });
};
