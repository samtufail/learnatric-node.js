module.exports = (connection, DataTypes) => {
  return connection.define("q_id_update", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    question_id: {
      type: DataTypes.INTEGER,
    },
    question_id_new: { type: DataTypes.INTEGER },
  }, {timestamps: false});
};