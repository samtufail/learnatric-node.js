const { Sequelize, DataTypes } = require("sequelize");
const {
  DATABASE_NAME,
  DATABASE_HOST,
  DATABASE_PASSWORD,
  DATABASE_USERNAME,
  DATABASE_PORT,
  DATABASE_ENV,
} = require("../constants");

const connection = new Sequelize(
  DATABASE_NAME,
  DATABASE_USERNAME,
  DATABASE_PASSWORD,
  {
    host: DATABASE_HOST,
    port: DATABASE_PORT,
    logging: false,
    dialect: "postgres",
  }
);

connection
  .authenticate()
  .then(() => {
    console.log(`Connected to ${DATABASE_ENV} Database Successfully!`);
  })
  .catch((err) => {
    console.error("Unable to connect to the database: ", err);
  });

const Parent = require("./Parent")(connection, DataTypes);
const Child = require("./Child")(connection, DataTypes);
const Question = require("./Question")(connection, DataTypes);
const Lesson = require("./Lesson")(connection, DataTypes);
const Domain = require("./Domain")(connection, DataTypes);
const DomainGrade = require("./DomainGrade")(connection, DataTypes);
const Standard = require("./Standard")(connection, DataTypes);
const Tag = require("./Tag")(connection, DataTypes);
const TagLesson = require("./LessonTag")(connection, DataTypes);
const TagQuestion = require("./QuestionTag")(connection, DataTypes);
const UserLesson = require("./UserLesson")(connection, DataTypes);
const Answer = require("./Answer")(connection, DataTypes);
const TagPerformance = require("./TagPerformance")(connection, DataTypes);
const TagRating = require("./TagRating")(connection, DataTypes);
const LessonQuestion = require("./LessonQuestion")(connection, DataTypes);
const QuestionIdUpdate = require("./QuestionIdUpdate")(connection, DataTypes);
const Assessment = require("./Assessment")(connection, DataTypes);
// const DateRef = require("./DateRef")(connection, DataTypes);
// TagRating.hasMany(Question)
// Question.belongsTo(TagRating, {
//   foreignKey: "p_tag",
//   constraints: false,
// })

Lesson.belongsToMany(Question, {
  through: LessonQuestion,
  foreignKey: "lesson_id",
});
Question.belongsToMany(Lesson, {
  through: LessonQuestion,
  foreignKey: "question_id",
});
Lesson.hasMany(LessonQuestion, {
  foreignKey: "lesson_id",
});
LessonQuestion.belongsTo(Lesson, {
  foreignKey: "lesson_id",
});
Question.hasMany(LessonQuestion, {
  foreignKey: "question_id",
});
LessonQuestion.belongsTo(Question, {
  foreignKey: "question_id",
});

Tag.hasMany(TagRating, {
  foreignKey: "tag_id",
  constraints: false,
  as: "TagRating",
});

TagRating.belongsTo(Tag, {
  foreignKey: "tag_id",
  constraints: false,
  as: "Tag",
});

Parent.hasMany(Child, {
  foreignKey: "parentId",
  constraints: false,
  as: "Children",
});

Child.belongsTo(Parent, {
  foreignKey: "parentId",
  constraints: false,
  as: "Parent",
});

Lesson.belongsTo(Tag, {
  foreignKey: "tag_id",
  constraints: false,
  as: "Tag",
});

Child.hasMany(TagPerformance, {
  foreignKey: "child_id",
  constraints: false,
  as: "child_Tag_Performance",
});

// TagPerformance.belongsTo(Tag, {
//   foreignKey: "tag_id",
//   // constraints: false,
// });

Answer.belongsTo(Question, {
  foreignKey: "question_id",
  constraints: false,
  as: "Answer_result_question",
});

Answer.belongsTo(Lesson, {
  foreignKey: "lesson_id",
  constraints: false,
  as: "Answer_result_lesson",
});

Answer.belongsTo(Child, {
  foreignKey: "child_id",
  constraints: false,
  as: "Answer_result_child",
});

module.exports = {
  connection,
  Parent,
  Child,
  Lesson,
  Tag,
  TagPerformance,
  Question,
  UserLesson,
  Answer,
  TagRating,
  TagLesson,
  TagQuestion,
  Domain,
  Standard,
  DomainGrade,
  LessonQuestion,
  QuestionIdUpdate,
  Assessment,
  // DateRef
};
