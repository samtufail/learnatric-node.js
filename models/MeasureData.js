module.exports = (connection, DataTypes) => {
  return connection.define(
    "measure_data",
    {
      m_d_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      question_id: { type: DataTypes.INTEGER },
      question_rating: { type: DataTypes.INTEGER },
      tag_id: { type: DataTypes.INTEGER },
      grade: { type: DataTypes.TEXT },
      domain_code: { type: DataTypes.TEXT },
    },
    { timestamps: false }
  );
};
