module.exports = (connection, DataTypes) => {
  return connection.define("Tag_rating", {
    tag_rating_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    child_id: { type: DataTypes.INTEGER },
    tag_id: { type: DataTypes.INTEGER },
    tag_rating: { type: DataTypes.FLOAT },
    grade: { type: DataTypes.TEXT}
  });
};
