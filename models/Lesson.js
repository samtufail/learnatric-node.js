module.exports = (connection, DataTypes) => {
  return connection.define(
    "Lesson",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      tag_id: {
        type: DataTypes.INTEGER,
      },
      lesson_name: {
        type: DataTypes.TEXT,
      },
      lesson_description: {
        type: DataTypes.TEXT,
      },
      standard_id: {
        type: DataTypes.TEXT,
      },
      grade: {
        type: DataTypes.TEXT,
      },
      question_rating: {
        type: DataTypes.TEXT,
      },
      i_do_url: {
        type: DataTypes.TEXT,
      },
      we_do_url1: {
        type: DataTypes.TEXT,
      },
      we_do_url2: {
        type: DataTypes.TEXT,
      },
      we_do_url3: {
        type: DataTypes.TEXT,
      },
      we_do_question_text: {
        type: DataTypes.TEXT,
      },
      question_type: {
        type: DataTypes.TEXT,
      },
      answer: {
        type: DataTypes.TEXT,
      },
      mc1: {
        type: DataTypes.TEXT,
      },
      mc2: {
        type: DataTypes.TEXT,
      },
      mc3: {
        type: DataTypes.TEXT,
      },
      mc4: {
        type: DataTypes.TEXT,
      },
    },
    { timestamps: false }
  );
};
