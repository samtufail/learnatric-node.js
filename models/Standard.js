module.exports = (connection, DataTypes) => {
  return connection.define("Standard", {
    standard_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    standard_group: { type: DataTypes.TEXT },
    standard_code: { type: DataTypes.TEXT },
    standard_description: { type: DataTypes.TEXT },
    grade: { type: DataTypes.TEXT },
    subject: { type: DataTypes.TEXT },
    domaine_name: { type: DataTypes.TEXT },
    domaine_code: { type: DataTypes.TEXT },
  });
};
