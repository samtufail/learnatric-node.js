module.exports = (connection, DataTypes) => {
  return connection.define(
    "Answer_Results",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      child_id: { type: DataTypes.INTEGER },
      lesson_id: { type: DataTypes.INTEGER, defaultValue: null },
      question_id: { type: DataTypes.INTEGER },
      text_answer: { type: DataTypes.TEXT },
      is_correct: { type: DataTypes.BOOLEAN },
      is_we_do: { type: DataTypes.BOOLEAN, defaultValue: false },
      tag_id: { type: DataTypes.INTEGER, defaultValue: null },
      domain_code: { type: DataTypes.TEXT, defaultValue: null },
      // date_time: { type: DataTypes.TEXT },
    },
    {
      timestamps: true,
    }
  );
};
