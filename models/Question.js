module.exports = (connection, DataTypes) => {
  return connection.define(
    "Question",
    {
      question_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        // autoIncrement: true
      },
      // lesson_id: { type: DataTypes.INTEGER },
      target: { type: DataTypes.TEXT },
      question_text: { type: DataTypes.TEXT },
      correct_answer: { type: DataTypes.TEXT },
      question_rating: { type: DataTypes.TEXT },
      p_tag: { type: DataTypes.TEXT },
      s_tag1: { type: DataTypes.TEXT },
      s_tag2: { type: DataTypes.TEXT },
      s_tag3: { type: DataTypes.TEXT },
      s_tag4: { type: DataTypes.TEXT },
      s_tag5: { type: DataTypes.TEXT },
      s_tag6: { type: DataTypes.TEXT },
      audio_url: { type: DataTypes.TEXT },
      illustration_url: { type: DataTypes.TEXT },
      question_type: { type: DataTypes.TEXT },
      mc1: { type: DataTypes.TEXT },
      mc2: { type: DataTypes.TEXT },
      mc3: { type: DataTypes.TEXT },
      mc4: { type: DataTypes.TEXT },
    },
    { timestamps: false }
  );
};
