module.exports = (connection, DataTypes) => {
  return connection.define(
    "Domain",
    {
      domaine_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      domaine_name: { type: DataTypes.TEXT },
      domaine_code: { type: DataTypes.TEXT },
      subject: { type: DataTypes.TEXT },
    },
    { timestamps: false }
  );
};
