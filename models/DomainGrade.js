module.exports = (connection, DataTypes) => {
  return connection.define("Domain_Grade", {
    domain_grade_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    domain_name: { type: DataTypes.TEXT },
    domain_code: { type: DataTypes.TEXT },
    grade: { type: DataTypes.INTEGER },
    subject: { type: DataTypes.TEXT },
  }, {timestamps: false});
};
