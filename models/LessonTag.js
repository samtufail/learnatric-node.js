module.exports = (connection, DataTypes) => {
  return connection.define("T_L_Relationship", {
    t_l_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    tag_id: { type: DataTypes.INTEGER },
    lesson_id: { type: DataTypes.INTEGER },
    tag_rating: { type: DataTypes.DOUBLE },
    priority: { type: DataTypes.INTEGER },
  });
};
