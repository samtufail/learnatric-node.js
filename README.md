# node_server

## This is the API for the Leanatric Web App
Follow these steps in order to run this server on your local machine for testing and development

## Getting Started

### Dependencies

* You will need to have PostgreSQL installed on your machine. Although for now since tha app inst launched yet you can use the Database that is already hosted on a AWS EC2 instance it will not always be like that. I recoment using pgAdmin and you can install everything all at once (postgres, postgres cli, and pgAdmin) by going to [https://www.postgresql.org/download/]. Once you have done this please reach out to me brian@learnatric.com and I will walk you through the connectiong to the hosted server in pgAdmin as you will need sensitive information (URL, password, username, ect.)

### Installing

* First clone this repo to your local machine ```git clone https://github.com/learnatric-App/node_server.git```
* Then switch to the ```local_testing``` branch by running the command ```git checkout local_testing```
* run ```npm install```
* Before running the start commond you will need to just double check a few things
  * Navigate to /database/index.js and make sure lines 5 - 11 are commented and lines 14-15 are uncommented
  * Navigate to /Stripe/modal.js and make sure lines 8 - 14 are commented and lines 2 - 5 are uncommented

### Executing program

* to start the devlopment server with nodemon listening for changes in the files run ```npm run start:dev```
  * this will start a server listening on ```localhost:8081``` if you already have something running on port ```8081``` you can change this in /server/index.js line 12 just be sure to change it back before submitting a pull request
