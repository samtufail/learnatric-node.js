const { UserLesson } = require("../models");

class UserLessonService {
  findUserLesson = async (user, lesson) => {
    try {
      const findUser = await User_Lessons.findOne({
        where: { child_id: user, lesson_id: lesson },
      });
      // console.log('find user: ', findUser)
      return findUser;
    } catch (err) {
      throw new Error(err);
    }
  };

  findAllUserLessons = async (user) => {
    try {
      const findAllLessonsByUser = await UserLesson.findAll({
        where: { child_id: user, problem_set: true },
        attributes: ["lesson_id", "times_attempted",],
        raw: true
      });
      return findAllLessonsByUser;
    } catch (err) {
      throw new Error(err);
    }
  };

  filterLessonsNotCompleted = async (grade, id) => {
    // console.log('grade: ', grade, 'id: ', id)
    try {
      const lesson = new LQ();
      const allLessons = await lesson.findAllByGradeLevel(grade);
      //find all records in User_lessons table related to that child_id
      const lessonsCompleted = await this.findAllUserLessons(id);
      const completeIdsMap = new Map();
      lessonsCompleted.forEach((completed) => {
        // console.log('lessons: ', completed.dataValues.lesson_id)
        completeIdsMap.set(
          completed.dataValues.lesson_id,
          completed.dataValues.lesson_id
        );
      });

      let lessonToFetch;
      for (let i = 0; i < allLessons.length; i++) {
        // console.log('All lessons ids:', allLessons[i].dataValues.id)
        if (!completeIdsMap.has(allLessons[i].dataValues.id)) {
          lessonToFetch = allLessons[i].dataValues.id;
          break;
        }
      }
      //this function and the following ternary handle the case that a child has already completed all the lessons for their grade level
      const pickRandomIndex = (min, max) => {
        return Math.floor(Math.random() * (max - min) + min);
      };
      const newLesson =
        lessonToFetch === undefined
          ? allLessons[pickRandomIndex(0, allLessons.length)].dataValues.id
          : lessonToFetch;

      const lessonToReturn = await lesson.find(newLesson);
      return lessonToReturn;
    } catch (e) {
      throw new Error(e);
    }
  };
  userLesson = async (data) => {
    try {
      const checkForRecord = await this.findUserLesson(
        data.child_id,
        data.lesson_id
      );

      if (!checkForRecord) {
        const newUserLesson = await User_Lessons.create({
          child_id: data.child_id,
          lesson_id: data.lesson_id,
          instruction_completed: data.instruction_completed,
          activity_completed: data.activity_completed,
          problem_set: data.problem_set,
          // start_date_time: data.start_date_time,
          // end_date_time: data.end_date_time,
        });
        return newUserLesson;
      } else {
        const updateData = {
          instruction_completed: data.instruction_completed,
          activity_completed: data.activity_completed,
          problem_set: data.problem_set,
        };
        const updateUserLesson = await User_Lessons.update(updateData, {
          where: { child_id: data.child_id, lesson_id: data.lesson_id },
        });
        return updateUserLesson;
      }
    } catch (e) {
      throw new Error(e);
    }
  };
}

module.exports = { UserLessonService };
