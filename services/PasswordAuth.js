const { Parent, Children, Lesson, Question } = require("../database/index");
const { LQ } = require("./lq");
const { UserLesson } = require("./UserLesson");
const { hashPW, matchPassword } = require("../Utils/HashPassword");

class PasswordAuth {
  findAccount = async (username, password) => {
    if (username.includes("@")) {
      //parent login with email address
      const account = await Parent.findOne({ where: { Email: username } });
      if (account === null) {
        return "Email doesn't exist";
      } else {
        const data = account.toJSON();
        const { password: originalPassword, salt } = data;
        const passwordMatched = await matchPassword(
          originalPassword,
          password,
          salt
        );
        if (passwordMatched) {
          const { password, salt, ...rest } = data;
          return { account: { ...rest }, type: "parent", isParent: true };
        } else {
          return "Invalid login details. Please confirm your username and password";
        }
      }
    } else {
      // child login with userName
      const account = await Children.findOne({ where: { username } });

      if (account === null) {
        return "Username doesn't exist";
      } else {
        const childData = account.toJSON();
        const {
          password: originalPassword,
          salt,
          isFirstLogin,
          id,
        } = childData;
        const passwordMatched = await matchPassword(
          originalPassword,
          password,
          salt
        );

        if (passwordMatched) {
          const getter = new LQ();
          const returnedGrade = account.grade;
          const lessonGrade =
            returnedGrade === "Third"
              ? "3"
              : returnedGrade === "Second"
              ? "2"
              : returnedGrade === "First"
              ? "1"
              : "0";
          const { password, salt, ...rest } = childData;
          if (isFirstLogin) {
            await Children.update({ isFirstLogin: false }, { where: { id } });
            const lesson = await Lesson.findOne({
              where: { grade: lessonGrade },
            });
            const lessonsAndQuestions = await getter.find(lesson.dataValues.id);
            return {
              account: { ...rest },
              type: "children",
              lessonsAndQuestions,
            };
          } else {
            const userLesson = new UserLesson();
            const lessonsAndQuestions =
              await userLesson.filterLessonsNotCompleted(lessonGrade, id);

            return {
              account: { ...rest },
              type: "children",
              lessonsAndQuestions,
            };
          }
        } else {
          return "Invalid login details. Please confirm your username and password";
        }
      }
    }
  };
}

module.exports = { PasswordAuth };
