const { Parent, Children } = require("../database/index");
const { hashPW } = require("../Utils/HashPassword");

class NewAccount {
  addParent = async (data) => {
    try {
      const checkForEmail = await Parent.findOne({
        where: { Email: data.Email },
      });

      if (checkForEmail === null) {
        const hashAndSalt = await hashPW(data.password, null);
        const newParent = await Parent.create({
          FirstName: data.FirstName,
          LastName: data.LastName,
          Email: data.Email,
          password: hashAndSalt.hashToSave,
          salt: hashAndSalt.genSalt,
          hear_about_us: data.hear_about_us,
          child_count: data.child_count,
          stripe_customer_id: data.stripe_customer_id,
          stripe_sub_id: data.stripe_sub_id,
        });
        return newParent;
      } else {
        return "Account for with this Email already exists";
      }
    } catch (e) {
      throw new Error(e);
    }
  };

  addParentStripeIds = async (custId, subId, parentId) => {
    try {
      const updatedParent = await Parent.update(
        {
          stripe_cutomer_id: custId,
          stripe_sub_id: subId,
        },
        {
          where: { id: parentId },
          returning: true,
          plain: true,
        }
      );
      return updatedParent;
    } catch (err) {
      throw new Error(err);
    }
  };

  addChild = async (data) => {
    try {
      const newChild = await Children.create({
        firstName: data.firstName,
        lastName: data.lastName,
        birthday: data.birthday,
        gender: data.gender,
        grade: data.grade,
        school: data.school,
        teachersName: data.teachersName,
        teachersEmail: data.teachersEmail,
        schoolDistrict: data.schoolDistrict,
        isOkayToEmailTeacher: data.isOkayToEmailTeacher,
        parentId: data.parentId,
        isFirstLogin: true,
      });
      return newChild;
    } catch (e) {
      throw new Error(e);
    }
  };

  findAllChildren = async (id) => {
    try {
      const allChildren = await Children.findAll({
        where: { parentId: id },
      });
      return allChildren;
    } catch (e) {
      throw new Error(e);
    }
  };

  createChildProfile = async (
    username,
    password,
    avatar_small,
    avatar_large,
    child_id
  ) => {
    try {
      const checkForUserName = await Children.findOne({
        where: { username: username },
      });
      if (checkForUserName === null) {
        const childHashAndSalt = await hashPW(password, null);

        const newChildProfile = await Children.update(
          {
            username: username,
            password: childHashAndSalt.hashToSave,
            salt: childHashAndSalt.genSalt,
            avatar_small: avatar_small,
            avatar_large: avatar_large,
          },
          {
            where: { id: child_id },
            returning: true,
            plain: true,
          }
        );
        return newChildProfile;
      } else {
        return "This Username Already Exists, Create a New One";
      }
    } catch (e) {
      throw new Error(e);
    }
  };

  updateChildLesson = async (id) => {
    try {
      const updatedChildProfile = await Children.increment(
        "learning_path_position",
        {
          where: { id },
          returning: true,
          plain: true,
        }
      );

      return updatedChildProfile[0];
    } catch (e) {
      throw new Error(e);
    }
  };

  getChildDetail = async (id) => {
    try {
      const child = await Children.findOne({
        where: { id },
        attributes: { exclude: ["password", "salt"] },
      });
      return child;
    } catch (e) {
      throw new Error(e);
    }
  };
}

module.exports = { NewAccount };
