const { Tag_Performance, Children } = require("../database/index");

class ChildrenData {
  getChildrenData = async (id) => {
    try {
      const allTags = await Children.findOne({
        where: {
          id: id,
        },
        include: [
          {
            model: Tag_Performance,
            as: "child_Tag_Performance",
          },
        ],
        order: [["child_Tag_Performance", "performance_id", "ASC"]],
      });
      return allTags;
    } catch (e) {
      throw new Error(e);
    }
  };
}

module.exports = { ChildrenData };
