const { Tag_Performance, Tag_rating } = require("../database/index");
const { tagCalc } = require("../Utils/TagCalculation");

class TagsRatingData {
  getTagPerformance = async (id) => {
    try {
      const allTags = await Tag_Performance.findAll({
        where: {
          child_id: id,
        },
        order: [["performance_id", "ASC"]],
      });

      return allTags;
    } catch (e) {
      throw new Error(e);
    }
  };
  addNewTagPerformance = async (data) => {
    try {
      const newRecord = await Tag_Performance.create(data);
      return newRecord;
    } catch (e) {
      throw new Error(e);
    }
  };

  getSpecificTags = async (tag, child) => {
    try {
      const specificTag = await Tag_rating.findOne({
        where: {
          tag_id: tag,
          child_id: child,
        },
      });
      return specificTag;
    } catch (e) {
      throw new Error(e);
    }
  };

  updateTagRating = async (data) => {
    console.log("data coming> ", data);
    const bool = data.isCorrect;
    const target = !data.is_we_do ? data.target : null;

    const tagToUpdate = await this.getSpecificTags(data.tag_id, data.child_id);
    // console.log('specific tag: ', tagToUpdate)
    const newScore = await tagCalc(
      tagToUpdate.dataValues.tag_rating,
      Number.parseInt(data.question_rating),
      bool,
      target
    );
    let updateData = {
      tag_rating: newScore,
    };
    try {
      const tagUpdateObj = {
        child_id: data.child_id,
        question_id: data.is_we_do ? data.lesson_id : data.question_id,
        question_rating: data.question_rating,
        lesson_id: data.lesson_id,
        tag_id: data.tag_id,
        tag_rating_start: tagToUpdate.dataValues.tag_rating,
        rating_new: newScore,
        is_correct: data.isCorrect,
        is_we_do: data.is_we_do,
      };
      const tagPerf = await this.addNewTagPerformance(tagUpdateObj);

      let allTags = await Tag_rating.update(updateData, {
        where: {
          child_id: data.child_id,
          tag_id: data.tag_id,
        },
      });
      return allTags;
    } catch (e) {
      throw new Error(e);
    }
  };
}

module.exports = { TagsRatingData };
