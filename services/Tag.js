const { Tags, Tag_Performance, Tag_rating } = require("../database/index");

class TagsData {
  getTags = async () => {
    try {
      const allTags = await Tags.findAll({});
      return allTags;
    } catch (e) {
      throw new Error(e);
    }
  };

  bulkInsertTagPerfomance = async (data) => {
    try {
      const tagPerfomance = await Tag_Performance.bulkCreate(data);
      return tagPerfomance;
    } catch (e) {
      throw new Error(e);
    }
  };

  bulkInsertTagRating = async (data) => {
    try {
      const tagRating = await Tag_rating.bulkCreate(data);
      return tagRating;
    } catch (e) {
      throw new Error(e);
    }
  };

  //for when Tags table gets dropped and need to import data again
  bulkInsertTags = async (data) => {
    try {
      const tags = await Tags.bulkCreate(data);
      return tags;
    } catch (e) {
      throw new Error(e);
    }
  };
}

module.exports = { TagsData };
