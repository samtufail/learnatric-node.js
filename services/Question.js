const { Question } = require("../database/index");

class FilterQuestion {
  filterQuestion = async (lesson_id) => {
    try {
      const question = await Question.findAll({
        where: {
          lesson_id: lesson_id,
        },
        order: [["question_rating", "ASC"]],
        limit: 5,
      });
      return question;
    } catch (e) {
      throw new Error(e);
    }
  };

  addBulkQuestions = async (questionFromJSON) => {
    try {
    } catch (error) {
      throw new Error(error);
    }
  };
}

module.exports = {
  FilterQuestion,
};
