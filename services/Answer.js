const { Answer_Results, Question, Lesson, Children } = require("../database");

class AnswerResult {
  addAnswerResult = async (data) => {
    // console.log('data: ', data)
    try {
      const newAnswerResult = await Answer_Results.create({
        child_id: data.child_id,
        lesson_id: data.lesson_id,
        question_id: data.is_we_do ? data.lesson_id : data.question_id,
        text_answer: data.text_answer,
        is_correct: data.isCorrect,
        is_we_do: data.is_we_do,
      });

      return newAnswerResult;
    } catch (e) {
      console.log("answerResultService addAnswerResult error ln-23", e);
      throw new Error(e);
    }
  };

  findAllAnswerResult = async (id) => {
    try {
      const answerResult = await Answer_Results.findAll({
        where: { answer_result_id: id },
      });
      return answerResult;
    } catch (e) {
      throw new Error(e);
    }
  };

  deleteAnswerResult = async (id) => {
    try {
      const deleteAnswerResult = await Answer_Results.destroy({
        where: { answer_result_id: id },
      });
      return deleteAnswerResult;
    } catch (e) {
      throw new Error(e);
    }
  };

  updateAnswerResult = async (data, id) => {
    let updatedata = {
      user_id: data.user_id,
      lesson_id: data.lesson_id,
      question_id: data.question_id,
      text_answer: data.text_answer,
      is_correct: data.is_correct,
      date_time: data.date_time,
    };
    try {
      let updateAnswerResult = await Answer_Results.update(updatedata, {
        where: { answer_result_id: id },
      });

      return updateAnswerResult;
    } catch (e) {
      throw new Error(e);
    }
  };

  getLessonQuestion = async (answer_result_id) => {
    try {
      const allLessonQuestion = await Answer_Results.findOne({
        where: {
          answer_result_id: answer_result_id,
        },
        include: [
          {
            model: Children,
            as: "Answer_result_child",
          },
          {
            model: Lesson,
            as: "Answer_result_lesson",
          },
          {
            model: Question,
            as: "Answer_result_question",
          },
        ],
      });
      return allLessonQuestion;
    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  };
}

module.exports = { AnswerResult };
