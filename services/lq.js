const { Lesson, Question } = require("../database/index");

class LQ {
  // addLesson = async (data) => {
  //   const lesson = await Lesson.create({
  //     tag_id: data.tag_id,
  //     lesson_name: data.lesson_name,
  //     lesson_description: data.lesson_description,
  //     standard_id: data.standard_id,
  //     grade: data.grade,
  //     tag_rating_avg: data.tag_rating_avg,
  //     i_do_url: data.i_do_url,
  //     we_do_url1: data.we_do_url1,
  //     we_do_url2: data.we_do_url2,
  //     we_do_url3: data.we_do_url3,
  //     we_do_question_text: data.we_do_question_text,
  //     question_type: data.question_type,
  //     answer: data.answer,
  //     mc1: data.mc1,
  //     mc2: data.mc2,
  //     mc3: data.mc3,
  //     mc4: data.mc4,

  //   })
  //   return lesson;
  // }
  // addQuestion = async (data) => {
  //   const question = await Question.create({
  //     // lesson1_id: data.lesson1_id,
  //     // lesson2_id: data.lesson2_id,
  //     target: data.target,
  //     question_text: data.question_text,
  //     answer_text: data.answer_text,
  //     question_rating: data.question_rating,
  //     p_tag: data.p_tag,
  //     // s_tag1: data.s_tag1,
  //     audio_url: data.audio_url,
  //     illustration_url: data.illustration_url,
  //     question_type: data.question_type,
  //     mc_1: data.mc_1,
  //     mc_2: data.mc_2,
  //     mc_3: data.mc_3,
  //     mc_4: data.mc_4,
  //     lesson_id: data.lesson_id,
  //   })
  //   return question
  // }

  getLessonQuestionData = async (id) => {
    try {
      const allLesson = await Lesson.findOne({
        where: {
          id: id,
        },
        include: [
          {
            model: Question,
            as: "Question",
          },
        ],
        order: [["Question", "question_id", "ASC"]],
      });
      return allLesson;
    } catch (e) {
      throw new Error(e);
    }
  };

  bulkCreateQuestion = async (questions) => {
    try {
      await Question.bulkCreate(questions);
    } catch (error) {
      throw new Error(error);
    }
  };
  bulkCreateLesson = async (lessons) => {
    try {
      await Lesson.bulkCreate(lessons);
    } catch (error) {
      throw new Error(error);
    }
  };

  findAllByGradeLevel = async (gradeLevel) => {
    try {
      const lessonsByGrade = await Lesson.findAll({
        where: { grade: gradeLevel },
      });
      return lessonsByGrade;
    } catch (err) {
      console.log("lq, findAllBygradeLevel error ln-94: ", err);
      throw new Error(err);
    }
  };

  find = async (lessonId) => {
    console.log("lessonID: ", lessonId);
    try {
      const lessonWithQuestions = await Lesson.findOne({
        include: [{ model: Question, as: "Question", where: { lesson_id: lessonId } }],
      });
      // const getter = new LQ();

      // const lessonWithQuestions = await getter.find(req.query.lesson_id)
      const {
        id,
        tag_id,
        lesson_name,
        i_do_url,
        we_do_url1,
        we_do_url2,
        we_do_url3,
        we_do_question_text,
        question_type,
        answer,
        mc1,
        mc2,
        mc3,
        mc4,
        question_rating,
      } = lessonWithQuestions;
      //  starting here is some temp logic to pick 5 random questions out of the array of questioins
      const length = lessonWithQuestions.Question.length;
      const a = [...Array(length).keys()];
      var n;
      var r = [];
      for (n = 1; n <= 5; ++n) {
        var i = Math.floor(Math.random() * (length - n) + 1);
        r.push(a[i]);
        a[i] = a[length - n];
      }
      const returnedQs = lessonWithQuestions.Question;
      const questions = [returnedQs[r[0]], returnedQs[r[1]], returnedQs[r[2]], returnedQs[r[3]], returnedQs[r[4]]];
      // ending here....
      const questionsArray = [];

      //this is tmp logic to test all questions for a lesson not just 5, remember to change (returnedQs.forEach) back to (questions.forEach)

      questions.forEach((question) => {
        // console.log('question: ', question);

        questionsArray.push({
          question_id: question.question_id,
          lesson_id: question.lesson_id,
          target: question.target,
          question_text: question.question_text,
          correct_answer: question.correct_answer,
          question_rating: question.question_rating,
          target: question.target,
          p_tag: question.p_tag,
          s_tags: [question.s_tag1, question.s_tag2, question.s_tag3, question.s_tag4, question.s_tag6],
          audio_url: question.audio_url,
          illustration_url: question.illustration_url,
          question_type: question.question_type,
          mc1: question.mc1,
          mc2: question.mc2,
          mc3: question.mc3,
          mc4: question.mc4,
        });
      });

      const returnObj = {
        id: id,
        tag_id: tag_id,
        lesson_name: lesson_name,
        i_do_url: i_do_url,
        we_dos: [
          {
            url: we_do_url1,
            we_do_question_text: we_do_question_text,
            questionType: question_type,
            answer: answer,
            mc1: mc1,
            mc2: mc2,
            mc3: mc3,
            mc4: mc4,
            tag_id: tag_id,
            question_rating: question_rating,
          },
          {
            url: we_do_url2,
            we_do_question_text: we_do_question_text,
            question_type: question_type,
            answer: answer,
            mc1: mc1,
            mc2: mc2,
            mc3: mc3,
            mc4: mc4,
            tag_id: tag_id,
            question_rating: question_rating,
          },
          {
            url: we_do_url3,
            we_do_question_text: we_do_question_text,
            questionType: question_type,
            answer: answer,
            mc1: mc1,
            mc2: mc2,
            mc3: mc3,
            mc4: mc4,
            tag_id: tag_id,
            question_rating: question_rating,
          },
        ],
        questions: questionsArray,
      };
      return returnObj;
    } catch (e) {
      console.log("lq find error ln-202", e);
      throw new Error(e);
    }
  };
}

module.exports = { LQ };
