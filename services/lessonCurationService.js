const { TagRating, UserLesson, Lesson, TagLesson } = require('../models');
const {
  subtractFromHighestAndDivide,
  createArrayOfLessonIds,
  createLessonsWithTagFocusObj,
  createTagsVariance,
  sortLessonsByVariance,
  updateVariance,
  filterAndPickNextLesson,
} = require('../Utils/lessonCurationUtils');

const { UserLessonService } = require('./UserLessonService');

class LessonCuration {
  fetchChildTagRatings = async (child) => {
    const { child_id, grade } = child;
    try {
      const findAndOrderTags = await TagRating.findAll({
        where: { child_id, grade },
        order: [['tag_rating', 'ASC']],
        raw: true,
      });
      // console.log('find and order tags: ', findAndOrderTags)
      return findAndOrderTags;
    } catch (e) {
      throw new Error(e);
    }
  };

  fetchLessonByGrade = async (child) => {
    const { grade } = child;
    try {
      const lessons = await Lesson.findAll({
        where: { grade },
      });
      // console.log('all grade lessons', lessons)
      return lessons;
    } catch (e) {
      console.log('lessonCurationService fetchLessonByGrade error', e);
      throw new Error(e);
    }
  };

  /**
  const { grade } = child;
    const gradeLevel = grade === 'Third' ? '3' : grade === 'Second' ? '2' : grade === 'First' ? '1' : '0'

  */

  fetchLessonByGradeForTesting = async (child) => {
    const { grade } = child;
    // const gradeLevel = grade === "Third" ? "3" : grade === "Second" ? "2" : grade === "First" ? "1" : "0";
    try {
      const lessons = await Lesson.findAll({
        where: { grade: gradeLevel },
        attributes: ['id'],
        order: [['id', 'ASC']],
      });
      return lessons;
    } catch (e) {
      console.log(
        'lessonCurationService fetchLessonByGradeForTesting error',
        e
      );
      throw new Error(e);
    }
  };

  fetchTLrelationships = async (lessons) => {
    try {
      const relationships = await TagLesson.findAll({
        where: { lesson_id: lessons },
        attributes: ['tag_id', 'lesson_id', 'priority'],
        raw: true,
      });
      // console.log('relationships: ', relationships);
      return relationships;
    } catch (e) {
      console.log('lessonCuratioinService fetchTLrelationships error', e);
      throw new Error(e);
    }
  };

  curateIdealLesson = async (child) => {
    // console.log('child: ', child);
    try {
      const userLessons = new UserLessonService();
      // returns all tag ratings in childs grade level sorted form lowest to highest rating
      const findLowsAndHigTagScore = await this.fetchChildTagRatings(child);
      const createTagsPriority = subtractFromHighestAndDivide(
        findLowsAndHigTagScore,
        findLowsAndHigTagScore[findLowsAndHigTagScore.length - 1],
        findLowsAndHigTagScore[0]
      );
      // get all lesson by childs grade level
      const fetchAllLessons = await this.fetchLessonByGrade(child);
      // create an array of justs the lesson ids
      const createLessonIdArray = createArrayOfLessonIds(fetchAllLessons);
      // fetch all the lesson to tag relationships with lesson_idf from the created array
      const lessonTagRelations = await this.fetchTLrelationships(
        createLessonIdArray
      );
      const focusMap = createLessonsWithTagFocusObj(lessonTagRelations);
      const variance = createTagsVariance(createTagsPriority, focusMap);
      // const lessonsTotalVariance = addTotalVariance(variance);
      const completedLessons = await userLessons.findAllUserLessons(
        child.child_id
      );
      if (completedLessons.length) {
        // for now for testing purposes this is not updating but actually deleteing the already completed lessons from the object
        const addToVarianceIfCompleted = updateVariance(
          variance,
          completedLessons
        );
      } else {
        console.log('no length');
      }
      const sortedBylowestVariation = sortLessonsByVariance(variance);
      // console.log('leeson id: ', sortedBylowestVariation[0][0])
      return Number.parseInt(sortedBylowestVariation[0].lesson_id);
    } catch (e) {
      console.log('lessonCurationService curateIdealLesson error', e);
      throw new Error(e);
    }
  };

  // this method is for curating lessons in sequential order for the purpose of debugging new lessons
  curateNextLessonForTesting = async (child) => {
    try {
      // console.log('child to fetch data for: ', child);
      const lessons = new UserLessonService();
      const getAllGradeLevelLessons = await this.fetchLessonByGradeForTesting(
        child
      );

      // console.log('lessons: ', getAllGradeLevelLessons)
      const completedLessons = await lessons.findAllUserLessons(child.child_id);
      // console.log('completed lessosns: ', completedLessons)
      const nextLessonId = filterAndPickNextLesson(
        completedLessons,
        getAllGradeLevelLessons
      );
      return nextLessonId;
    } catch (e) {
      console.log('lessonCurationService curateNextLessonForTesting error', e);
      throw new Error(e);
    }
  };
}

module.exports = { LessonCuration };
