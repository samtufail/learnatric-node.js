const Stripe = require("stripe");

const { STRIPE_SK, STRIPE_MONTHLY_PRICE, STRIPE_YEARLY_PRICE } = require("../constants");

const stripe = Stripe(STRIPE_SK);

const createCustomer = async (paymentMethodID, email) => {
  try {
    const customer = await stripe.customers.create({
      payment_method: paymentMethodID,
      email: email,
      invoice_settings: {
        default_payment_method: paymentMethodID,
      },
    });
    return customer;
  } catch (err) {
    console.error('stripe create customer error: ', err);
    return err;
  }
};

const createSubscription = async (customerId, planType, quantity) => {
  const priceID = planType === "YEARLY" ? STRIPE_YEARLY_PRICE : STRIPE_MONTHLY_PRICE;
  const subscription = await stripe.subscriptions.create({
    customer: customerId,
    items: [{ price: priceID, quantity: quantity }],
    expand: ["latest_invoice.payment_intent"],
    trial_period_days: 30,
  });
  return subscription;
};

const updateQuantity = async (subId, quantity) => {
  const update = await stripe.subscriptions.update(subId, 
    {
      quantity: quantity,
    }
  )
  return update;
}

const updatePaymentMethod = async (subId, custId, paymentMethod, email) => {
  const createPaymentMethod = await stripe.paymentMethods.attach(
    paymentMethod.id,
    {customer: custId}
  );
  const paymentUpdate = await stripe.customers.update(
    custId,
    {
      email: email,
      invoice_settings: {
        default_payment_method: paymentMethod.id,
      },
    }
  );
  return paymentUpdate
}

const checkSubscription = async (subId) => {
  const subscription = await stripe.subscriptions.retrieve(subId);
  return subscription;
}

const fetchCustomer = async (customerId) => {
  const customer = await stripe.customers.retrieve(customerId);
  return customer;
}

const cancelSubscription = async (subId) => {
  try {
    const cancel = await stripe.subscriptions.update(subId, { cancel_at_period_end: true });
    return cancel;
  } catch (err) {
    console.log('stripe cancel subscription error: ', err);
    throw new Error(err);
  }
}

const resumeSubscription = async (subId) => {
  try {
    const resume = await stripe.subscriptions.update(subId, { cancel_at_period_end: false });
    return resume;
  } catch (err) {
    throw new Error(err)
  }
}

module.exports = {
  createCustomer,
  createSubscription,
  checkSubscription,
  fetchCustomer,
  updateQuantity,
  updatePaymentMethod,
  cancelSubscription,
  resumeSubscription
};
