const express = require("express");
const router = express.Router();

const { errorCatcher } = require("../middlewares/errors");
const { UserMetrics, AverageTagChange } = require("../controllers/userMetrics");

router.get('/user-metrics', errorCatcher(UserMetrics));
router.get('/tag-change', errorCatcher(AverageTagChange));

module.exports = router;