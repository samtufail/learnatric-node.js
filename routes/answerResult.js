const express = require("express");
const router = express.Router();

const { errorCatcher } = require("../middlewares/errors");
const { addAnswerResult } = require("../controllers/answerResult");

router.post("/answerResult", errorCatcher(addAnswerResult));

module.exports = router;
