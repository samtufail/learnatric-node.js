const express = require("express");
const { SyncDatabase, ForceSyncDatabase, tester, BulkCreate } = require("../controllers/database");
const { errorCatcher } = require("../middlewares/errors");

const router = express.Router();

router.get("/sync-database", errorCatcher(SyncDatabase));
router.get("/force-sync-database", errorCatcher(ForceSyncDatabase));
router.get("/bulk-create", BulkCreate)
router.get("/test", errorCatcher(tester));

module.exports = router;
