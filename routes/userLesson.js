const express = require("express");
const router = express.Router();

const { errorCatcher } = require("../middlewares/errors");
const {
  addUserLesson,
  updateUserLesson,
  updateTrainWobble,
} = require("../controllers/userLesson");

router.post("/userLesson", errorCatcher(addUserLesson));
router.put("/userLesson/:userLessonID", errorCatcher(updateUserLesson));
router.put("/trainwobble", errorCatcher(updateTrainWobble));

module.exports = router;
