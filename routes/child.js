const express = require("express");
const router = express.Router();

const { errorCatcher } = require("../middlewares/errors");
const {
  addChild,
  getParentChilds,
  updateChild,
  setChildToys,
} = require("../controllers/child");

router.post("/child", errorCatcher(addChild));
router.post("/child/set-toys", errorCatcher(setChildToys));
router.get("/childs", errorCatcher(getParentChilds));
router.put("/child", errorCatcher(updateChild));

module.exports = router;
