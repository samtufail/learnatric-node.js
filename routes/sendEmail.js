const express = require("express");
const router = express.Router();

const { errorCatcher } = require("../middlewares/errors");
const { SignupEmail } = require("../controllers/sendEmail");

router.post('/email-signup', errorCatcher(SignupEmail))

module.exports = router;