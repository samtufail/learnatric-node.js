const express = require("express");
const router = express.Router();


const { errorCatcher } = require("../middlewares/errors");
const { updateTP } = require("../controllers/tagPerformance");

router.post("/tag-performance", errorCatcher(updateTP));

module.exports = router;
