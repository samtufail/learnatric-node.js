const express = require("express");
const router = express.Router();

const { errorCatcher } = require("../middlewares/errors");
const {
  getTagsForDomain,
  getQsForTag,
  getAssessmentQuestions,
  updateTagsAndAnswers,
} = require("../controllers/assesment");

router.get("/domain-tags", errorCatcher(getTagsForDomain));
router.get("/question-by-tag", errorCatcher(getQsForTag));
router.get("/assessment-questions", errorCatcher(getAssessmentQuestions));
router.post("/tags-answers", errorCatcher(updateTagsAndAnswers));

module.exports = router;
