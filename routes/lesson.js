const express = require('express');

const router = express.Router();

const { errorCatcher } = require('../middlewares/errors');
const { getLessons } = require('../controllers/lesson');
const { fetchLessonQuestions } = require('../Utils/testLessonQuestions');

router.get('/lesson', errorCatcher(getLessons));
router.post('/lesson-test', errorCatcher(fetchLessonQuestions));

module.exports = router;
