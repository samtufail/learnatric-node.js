const express = require("express");

const child = require("./child");
const parent = require("./parent");
const lesson = require("./lesson");
const tag = require("./tag");
const tagRating = require("./tagRating");
const tagPerformance = require("./tagPerformance");
const question = require("./question");
const answerResult = require("./answerResult");
const userLesson = require("./userLesson");
const database = require("./database");
const parentDashboard = require("./parentDashboard");
const sendEmail = require("./sendEmail");
const accountSettings = require("./accountSettings");
const userMetrics = require("./userMetrics");
const assesment = require("./assesment");

module.exports = [].concat(
  child,
  parent,
  lesson,
  tag,
  tagRating,
  tagPerformance,
  question,
  answerResult,
  userLesson,
  database,
  parentDashboard,
  sendEmail,
  accountSettings,
  userMetrics,
  assesment
);
