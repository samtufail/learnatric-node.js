const express = require("express");
const router = express.Router();

const { fetchChildDomainData, switchToKid, switchToParent } = require('../controllers/parentDashboard');
const { errorCatcher } = require("../middlewares/errors");

router.get('/childrenTagData', errorCatcher(fetchChildDomainData));
router.get('/switch-to-child', errorCatcher(switchToKid));
router.get('/switch-to-parent', errorCatcher(switchToParent));

module.exports = router;