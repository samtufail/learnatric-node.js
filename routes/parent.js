const express = require("express");
const router = express.Router();

const { errorCatcher } = require("../middlewares/errors");
const { 
  SignUp, 
  Login, 
  CancelSub, 
  UpdateCard,
  BillingInfo, 
  ChildrenUpdate, 
  FetchChildData,
  ResumeSub } = require("../controllers/parent");

router.post("/parent/signup", errorCatcher(SignUp));
router.post("/user/login", errorCatcher(Login));
router.get("/parent/billing", errorCatcher(BillingInfo));
router.get("/parent/child-names", errorCatcher(FetchChildData));
router.put("/parent/billing-children-update", errorCatcher(ChildrenUpdate));
router.put("/parent/update-card", errorCatcher(UpdateCard));
router.put("/parent/cancel-subscription", errorCatcher(CancelSub));
router.put("/parent/resume-subscription", errorCatcher(ResumeSub));

module.exports = router;
