const express = require("express");
const router = express.Router();

const { errorCatcher } = require("../middlewares/errors");
const { addTagsFromJSON } = require("../controllers/tag");

router.post("/tags", errorCatcher(addTagsFromJSON));

module.exports = router;
