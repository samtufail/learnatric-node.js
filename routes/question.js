const express = require("express");
const router = express.Router();

const { errorCatcher } = require("../middlewares/errors");
const {
  curatedQuestion,
  fetchQuestionById,
} = require("../controllers/question");

router.get("/curate-question", errorCatcher(curatedQuestion));
router.get("/fetch-question", errorCatcher(fetchQuestionById));

module.exports = router;
