const express = require("express");
const router = express.Router();

const { errorCatcher } = require("../middlewares/errors");
const { 
  UpdateParentPassword, 
  UpdateChildPassword, 
  UpdateEmail, 
  UpdateChildUsername,
  UpdatePhoneNumber,
  ForgotPassword } = require("../controllers/accountSettings");

router.put("/account/update-password-parent", errorCatcher(UpdateParentPassword));
router.put("/account/update-password-child", errorCatcher(UpdateChildPassword));
router.put("/account/update-email", errorCatcher(UpdateEmail));
router.put("/account/update-child-username", errorCatcher(UpdateChildUsername));
router.put("/account/update-phone", errorCatcher(UpdatePhoneNumber));
router.put("/account/forgot-password", errorCatcher(ForgotPassword));

module.exports = router;