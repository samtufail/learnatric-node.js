const router = require("express").Router();
const { createUserLesson } = require("../controllers/userLesson");
const { errorCatcher } = require("../middlewares/errors");

router.post("/userlesson", createUserLesson);

module.exports = router;
