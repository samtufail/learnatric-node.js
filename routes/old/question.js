const router = require("express").Router();
const { errorCatcher } = require("../middlewares/errors");
const {
  createQuestion,
  bulkCreateQuestion,
  answerQuestion,
} = require("../controllers/createLQTags");
const { filterQuestion } = require("../controllers/question");

const { auth } = require("../middlewares/auth");

router.post("/question", auth, createQuestion);
router.post("/questions", bulkCreateQuestion);
router.get("/filter-question", filterQuestion);
router.post("question/answer-question", answerQuestion);

module.exports = router;
