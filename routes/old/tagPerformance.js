const router = require("express").Router();
const { errorCatcher } = require("../middlewares/errors");
const {
  getTagPerformanceData,
  updateTagRating,
} = require("../controllers/tagPerformance");

const { auth } = require("../middlewares/auth");

router.get("/fetch-tag-perfomance", getTagPerformanceData);

router.post("/tag/update-tag-performance", updateTagRating);

module.exports = router;
