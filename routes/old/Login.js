const router = require("express").Router();
const { loginController } = require("../controllers/loginController");
const { errorCatcher } = require("../middlewares/errors");

router.post("/login", loginController);

module.exports = router;
