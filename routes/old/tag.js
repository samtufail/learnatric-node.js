const router = require("express").Router();
const { errorCatcher } = require("../middlewares/errors");
const {
  createQuestion,
  bulkCreateQuestion,
  createLesson,
  bulkCreateLesson,
  bulkCreateTags,
} = require("../controllers/createLQTags");

router.post("/tags", bulkCreateTags);

module.exports = router;
