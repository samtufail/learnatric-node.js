const express = require("express");

const login = require("./Login");
const lesson = require("./lesson");
const parent = require("./parent");
const question = require("./question");
const children = require("./children");
const tagPerformance = require("./tagPerformance");
const tag = require("./tag");
const userlesson = require("./userLesson");
const answer = require("./answers");

module.exports = [].concat(
  login,
  lesson,
  parent,
  question,
  children,
  tag,
  tagPerformance,
  userlesson,
  answer
);
