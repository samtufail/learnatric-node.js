const router = require("express").Router();
const { errorCatcher } = require("../middlewares/errors");
const {
  //   getLessonQuestionData,
  createLesson,
  bulkCreateLesson,
} = require("../controllers/createLQTags");

const { auth } = require("../middlewares/auth");

router.post("/lesson", createLesson);
router.post("/lessons", bulkCreateLesson);
// router.get('/fetch-lesson-question', getLessonQuestionData)

module.exports = router;
