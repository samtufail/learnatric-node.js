const router = require("express").Router();
const { errorCatcher } = require("../middlewares/errors");
const {
  newChild,
  fetchChildren,
  createChildProfile,
  subscription,
  passLesson,
  getChildDetail,
} = require("../controllers/createAccountController");
const { getChildTagData } = require("../controllers/children");

const { auth } = require("../middlewares/auth");

router.get("/children/", getChildDetail);
router.post("/children/newchild", newChild);
router.get("/children/fetch-children", fetchChildren);
router.post("/children/create-child-profile", createChildProfile);
router.post("/children/pass-my-lesson", passLesson);
router.post("/children/sub", auth, subscription);
router.get("/children/tag-child-data", getChildTagData);

module.exports = router;
