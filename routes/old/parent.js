const router = require("express").Router();
const { errorCatcher } = require("../middlewares/errors");
const {
  newParent,
  migrate,
  subscription,
} = require("../controllers/createAccountController");

router.post("/parent/subscription", subscription);

router.post("/parent/new-parent", newParent);

router.get("/parent/migrate", migrate);

module.exports = router;
