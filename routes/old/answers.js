const router = require("express").Router();
const { errorCatcher } = require("../middlewares/errors");
const {
  createAnswerResult,
  fetchAnswerResult,
  deleteAnswerResult,
  updateAnswerResult,
  getLessonQuestionData,
} = require("../controllers/answers");

router.post("/create_answer-result", createAnswerResult);
router.get("/fetch-answer-result", fetchAnswerResult);
router.post("/delete-answer-result", deleteAnswerResult);
router.post("/update-answer-result", updateAnswerResult);
router.get("/lesson-question-answer-result", getLessonQuestionData);

module.exports = router;
