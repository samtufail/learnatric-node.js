const express = require("express");
const cors = require("cors");
// const morgan = require("morgan");
const routes = require("./routes");
const app = express();
const { PORT, HOSTNAME } = require("./constants");
const { welcome, error404, errorHandler } = require("./middlewares/errors");

// const corsOptions = {
//   origin: "*",
//   methods: ["GET", "HEAD", "PUT", "PATCH", "POST", "DELETE"],
//   preflightContinue: false,
//   optionsSuccessStatus: 200,
// };

// app.use(morgan("dev"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.all("/", welcome);
app.all("/api/v1", welcome);
app.use("/api/v1", routes);
app.use(errorHandler);
app.use(error404);

app.listen(PORT, () => {
  console.log("Server Running At " + HOSTNAME + PORT);
});
