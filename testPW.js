const bcrypt = require('bcrypt')


const createNewPW = (word) => {
  return bcrypt.hashSync(word, 6);
}

console.log(createNewPW("password"))