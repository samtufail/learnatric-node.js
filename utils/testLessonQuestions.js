const { Lesson, Question, Tag } = require('../models');
const fs = require('fs');

const fetchLessonQuestions = async (req, res) => {
  try {
    const tags = await Tag.findAll({
      attributes: [["tag_id", "id"]], 
      raw: true
    })
    const tagIds = tags.map(tag => tag.id)
    const lessons = await Lesson.findAll({
      attributes: [["id", "lesson_id"], ["tag_id", "tag"]], 
      raw: true
    })
    // console.log('lessons: ', lessons)
    const noTagsForLessons = [];
    lessons.forEach(lesson => {
      if ( tagIds.indexOf(lesson.tag) < 0 ) {
        noTagsForLessons.push(lesson)
      }
    })
    // console.log('noTagsForLessons: ', noTagsForLessons)
    const questions = await Question.findAll({
      attributes: [
        ["question_id", "question_id"],
        ["p_tag", "p"],
        ["s_tag1", "s1"],
        ["s_tag2", "s2"],
        ["s_tag3", "s3"],
        ["s_tag4", "s4"],
        ["s_tag5", "s5"],
        ["s_tag6", "s6"],
      ],
      raw: true
    })
    // console.log('questions: ', questions)
    const questionsWithArray = questions.map(question => {
      question = {
        id: question.question_id,
        tags: [
          question.p,
          question.s1 ? question.s1 : null,
          question.s2 ? question.s2 : null,
          question.s3 ? question.s3 : null,
          question.s4 ? question.s4 : null,
          question.s5 ? question.s5 : null,
          question.s6 ? question.s6 : null
        ]
      }
      return question
    })
    const questionsWithSameTagMoreThanOnce = [];
    const questionsWithNumsArr = questionsWithArray.map((question, idx) => {
      let tags  = question.tags
      let tagsWithNoNull = []
      let length = question.tags.length
      for (let i = 0; i < tags.length; i++) {
        if ( tags[i] === null ) {
          continue;
        } else if ( tagsWithNoNull.indexOf(Number.parseInt(tags[i])) >= 0 ) {
          question.tagThatAppearsMoreThanOnce = tags[i]
          // console.log('question: ', question)
          questionsWithSameTagMoreThanOnce.push(question)
        } else {
          tagsWithNoNull.push(Number.parseInt(tags[i]))
        }
      }
      question.tags = tagsWithNoNull
      return question
    })
    console.log('more than once: ', questionsWithSameTagMoreThanOnce)
    // console.log('q with array: ', questionsWithNumsArr)
    const questiondWithNoTags = []
    questionsWithNumsArr.forEach(question => {
      let tags  = question.tags
      for (let i = 0; i < tags.length; i++) {
        if ( tagIds.indexOf(tags[i]) < 0 ) {
          delete question.tagThatAppearsMoreThanOnce
          question.theTagThatDoesNotExist = tags[i]
          // console.log('questionWithNonExistingTag: ', question)
          questiondWithNoTags.push(question)
        }
      }
    })
    console.log('questiondWithNoTags: ', questiondWithNoTags)
  } catch (err) {
    console.error('error fetching lessons and questions', err)
  }
}

module.exports = {
  fetchLessonQuestions
}