const { QueryTypes } = require("sequelize");
const moment = require("moment"); // require
const { connection, DomainGrade, TagRating, Tag } = require("../models");

const getAvgTagScoresByDomain = async (date, child_id) => {
  try {
    const averages = await connection.query(
      `select tr.child_id, t.domaine, '${date}' as wk_ending,
        avg(case when sub.rating_new is null then tr.tag_rating else sub.rating_new end) avg_tag_rating
        from public."Tag_ratings" tr -- current tag ratings
          join public."Tags" t on tr.tag_id  = t.tag_id
          left join -- Latest Tag score before week began
            (select child_id, tag_id, rating_new
            from public."Tag_Performances" tp
            where performance_id in
              (select max(performance_id) performance_id
              from public."Tag_Performances" tp
                join public."Children" as c on tp.child_id = c.id
              where tp."createdAt" <= '${date}'
              group by child_id, tag_id)
            union -- Beginning tag score for ones that have been tested
            select child_id, tag_id, tag_rating_start as rating_new
            from public."Tag_Performances" tp
            where performance_id in
            (select min(performance_id) performance_id
              from public."Tag_Performances" tp
                join public."Children" as c on tp.child_id = c.id
              where tp."createdAt" > '${date}'
              group by child_id, tag_id)) sub
            on tr.child_id = sub.child_id and tr.tag_id =sub.tag_id
      where tr.child_id = ${child_id}  -- specify which student to run it for
      group by tr.child_id, t.domaine`,
      {
        type: QueryTypes.SELECT,
        raw: true,
      }
    );
    return averages;
  } catch (err) {
    console.log("get average tag score by domain error: ", err);
  }
};

const fetchDomainAvgLast5 = async (child_id) => {
  const currentWeek = moment()
    .clone()
    .endOf("week")
    .add(1, "days")
    .format("YYYY-MM-DD");
  const lastWeek = moment()
    .subtract(7, "days")
    .clone()
    .endOf("week")
    .add(1, "days")
    .format("YYYY-MM-DD");
  const threeWeeks = moment()
    .subtract(14, "days")
    .clone()
    .endOf("week")
    .add(1, "days")
    .format("YYYY-MM-DD");
  const fourWeeks = moment()
    .subtract(21, "days")
    .clone()
    .endOf("week")
    .add(1, "days")
    .format("YYYY-MM-DD");
  const fiveWeeks = moment()
    .subtract(28, "days")
    .clone()
    .endOf("week")
    .add(1, "days")
    .format("YYYY-MM-DD");
  const weeksPromiseArray = [
    getAvgTagScoresByDomain(currentWeek, child_id),
    getAvgTagScoresByDomain(lastWeek, child_id),
    getAvgTagScoresByDomain(threeWeeks, child_id),
    getAvgTagScoresByDomain(fourWeeks, child_id),
    getAvgTagScoresByDomain(fiveWeeks, child_id),
  ];
  let averages = await Promise.all(weeksPromiseArray);
  return averages;
};

const averageTheAverages = (averages, domains) => {
  let newAvgArray = [];
  for (let i = 0; i < averages.length; i++) {
    let sum = 0;
    let length = averages[i].length;
    let weekAverages = [];
    averages[i].forEach((week, idx) => {
      if (idx === length - 1) {
        sum += week.avg_tag_rating;
        weekAverages = [
          ...weekAverages,
          week,
          {
            domaine: "Average of All Domains",
            wk_ending: week.wk_ending,
            avg_tag_rating: Number.parseFloat(
              (sum / domains.length).toFixed(2)
            ),
          },
        ];
        newAvgArray.push(weekAverages);
      } else {
        if (domains.indexOf(week.domaine) > -1) {
          sum += week.avg_tag_rating;
          weekAverages = [...weekAverages, week];
        }
      }
    });
  }
  // console.log('new week averages: ', newAvgArray)
  return newAvgArray;
};

const formatAllAverages = (allAverages) => {
  //this is for google charts
  // const formattedArray = [];
  // let labelsArray = ['Week Ending Date'];
  // for (let i = 0; i < allAverages[0].length; i++) {
  //   labelsArray = [...labelsArray, allAverages[0][i].domaine]
  // }
  // formattedArray.push(labelsArray)
  // for (let j = 0; j < allAverages.length; j++) {
  //   let weekArray = [moment(allAverages[j][0].wk_ending).format("MMM Do YY")];
  //   let length = allAverages[j].length;
  //   allAverages[j].forEach((week, idx) => {
  //     if ( labels.indexOf(moment(week.wk_ending).format('MMM Do YY')) === -1) {
  //       labels = [...labels, moment(week.wk_ending).format('MMM Do YY')]
  //     }
  //     if ( idx === length - 1 ) {
  //       weekArray = [...weekArray, Number.parseFloat(week.avg_tag_rating.toFixed(2))]
  //       formattedArray.push(weekArray)
  //     } else {
  //       weekArray = [...weekArray, Number.parseFloat(week.avg_tag_rating.toFixed(2))]
  //     }
  //   })
  // }
  // return formattedArray;

  // this is for charts js
  const colors = [
    {
      backgroundColor: `rgb(235, 64, 52)`,
      borderColor: "rgba(235, 64, 52, 0.5)",
    },
    {
      backgroundColor: "rgb(44, 27, 204)",
      borderColor: "rgba(44, 27, 204, 0.5)",
    },
    {
      backgroundColor: "rgb(57, 204, 27)",
      borderColor: "rgba(57, 204, 27, 0.5)",
    },
    {
      backgroundColor: "rgb(230, 148, 16)",
      borderColor: "rgba(230, 148, 16, 0.5)",
    },
    {
      backgroundColor: "rgb(235, 228, 28)",
      borderColor: "rgba(235, 228, 28, 0.5)",
    },
    {
      backgroundColor: "rgb(240, 26, 225)",
      borderColor: "rgba(240, 26, 225, 0.5)",
    },
    {
      backgroundColor: "rgb(26, 232, 215)",
      borderColor: "rgba(26, 232, 215, 0.5)",
    },
  ];
  let datasets = [];
  let labels = [];
  let tempDataHolder = {};
  for (let j = 0; j < allAverages.length; j++) {
    let length = allAverages[j].length;
    allAverages[j].forEach((week, idx) => {
      if (labels.indexOf(moment(week.wk_ending).format("MMM Do YY")) === -1) {
        labels = [...labels, moment(week.wk_ending).format("MMM Do YY")];
      }
      if (!tempDataHolder[`${week.domaine}`]) {
        tempDataHolder[`${week.domaine}`] = [week.avg_tag_rating.toFixed(1)];
      } else {
        tempDataHolder[`${week.domaine}`] = [
          ...tempDataHolder[`${week.domaine}`],
          week.avg_tag_rating.toFixed(1),
        ];
      }
    });
  }

  // console.log('tempDataHolder', tempDataHolder)
  // console.log('formattedArray: ', labels)

  let count = 0;
  for (key in tempDataHolder) {
    datasets.push({
      label: key,
      data: tempDataHolder[key].reverse(),
      backgroundColor: colors[count].backgroundColor,
      borderColor: colors[count].borderColor,
    });
    count++;
  }

  let data = {
    labels: labels.reverse(),
    datasets,
  };
  // console.log('data', data);
  return data;
};

const getTopAndBottom3 = async (child_id, grade) => {
  try {
    const allTagRatings = await TagRating.findAll({
      where: { child_id: child_id, grade: grade },
      order: [["tag_rating", "ASC"]],
      include: {
        model: Tag,
        as: "Tag",
        attributes: ["tags", "domaine", "domain_code", "standard_description"],
      },
      raw: true,
    });
    // console.log("all ratings: ", allTagRatings);
    const topAndBottom3 = {
      bottom: [allTagRatings[0], allTagRatings[1], allTagRatings[2]],
      top: [
        allTagRatings[allTagRatings.length - 1],
        allTagRatings[allTagRatings.length - 2],
        allTagRatings[allTagRatings.length - 3],
      ],
    };
    return topAndBottom3;
  } catch (err) {
    console.log("getting top and bottom 3 ratings error: ", err);
  }
};

const getDomainsByGarde = async (grade) => {
  const domains = await DomainGrade.findAll({
    where: { grade: grade },
    attributes: ["domain_name"],
    raw: true,
  });
  const justNames = domains.map((domain) => domain.domain_name);
  // console.log('jjust names: ',justNames);
  return justNames;
};

// const timeSpentAndLessonCount = async (child_id) => {
//   try {
//     const timesAndLessonCount = await connection.query(
//       `select child_id, "LAST_DT_IN_WK", count(lesson_id) Lesson_count, sum(lesson_time) lesson_time
//         from
//           (select child_id, ul.lesson_id, dr."LAST_DT_IN_WK" ,
//             case when ul."updatedAt" - ul."createdAt" > '15:00.000' then '15:00.000' else ul."updatedAt" - ul."createdAt" end as lesson_time
//           from public."User_Lessons" ul
//             join public."Children" as c on ul.child_id = c.id
//             join date_ref dr on cast(ul."updatedAt" as date) = dr."DT_ID"
//           where child_id = ${child_id}     -- specify which student to run it for
//             and problem_set = true) s1
//         group by child_id, "LAST_DT_IN_WK"
//       ;`,
//       {
//         type: QueryTypes.SELECT,
//         raw: true,
//       }
//     );
//     for ( let week of timesAndLessonCount ) {
//       let hours = week.lesson_time.hours ? week.lesson_time.hours.toString() : '00';
//       let minutes = week.lesson_time.minutes ? week.lesson_time.minutes.toString() : '00';
//       let seconds = week.lesson_time.seconds ? week.lesson_time.seconds.toString() : '00';
//       let parseInterval = await connection.query(
//         `SELECT EXTRACT(EPOCH FROM INTERVAL '${hours} hours ${minutes} minutes ${seconds} seconds');`,
//         {
//           type: QueryTypes.SELECT,
//           raw: true,
//           plain: true
//         }
//       );
//       let timeInMinutes = Math.ceil((parseInterval.date_part / 60).toFixed());
//       week.LAST_DT_IN_WK = moment(week.LAST_DT_IN_WK).format('MMM Do YY');
//       week.lesson_time = timeInMinutes === 1 ? `${timeInMinutes} minute` : `${timeInMinutes} minutes`;
//     };
//     return timesAndLessonCount;
//   } catch (err) {
//     console.error(err);
//     throw new Error(err);
//   }
// }

module.exports = {
  getTopAndBottom3,
  fetchDomainAvgLast5,
  averageTheAverages,
  getDomainsByGarde,
  formatAllAverages,
  // timeSpentAndLessonCount
};
