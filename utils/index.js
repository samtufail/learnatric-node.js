const httpStatus = require("http-status");
const JWT = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const parser = require("csv-parser");
const { QueryTypes } = require("sequelize");
const {
  connection,
  Tag,
  Question,
  Lesson,
  TagLesson,
  TagRating,
  Parent,
  Child,
  DomainGrade,
  Domain,
  LessonQuestion,
  QuestionIdUpdate,
  TagPerformance,
  Answer,
  CountingCard,
  Geometry,
  MeasureData,
  NumbersBase,
  NumbersFractions,
  OperationAlgebraic,
  Assessment,
  // DateRef
} = require("../models");

const {
  JWT_SECRET_KEY,
  LESSON,
  QUESTION,
  TAG,
  RELATION,
  RATINGS,
  PARENTS,
  CHILDREN,
  DOMAIN_GRADE,
  DOMAIN,
  QUESTIONIDUPDATE,
  LESSONQUESTION,
  ANSWER_RESULT,
  PERFORMANCE,
  DATE,
  COUNTING_CARD,
  OPERATIONS_ALGEBRAIC,
  NUMBERS_BASE,
  MEASURE_DATA,
  GEOMETRY,
  NUMBERS_FRACTIONS,
  ASSESSMENT,
} = require("../constants");

const fs = require("fs");

const sendResponse = (response, type, statusCode, message, data = null) => {
  return response.status(statusCode).json({
    status: type,
    code: statusCode,
    statusMessage: httpStatus[statusCode],
    message: message,
    result: data,
  });
};

const JWT_SignIn = async (payload) => {
  const jwtKEY = await JWT.sign(payload, JWT_SECRET_KEY, { expiresIn: "7d" });
  return jwtKEY;
};

const JWT_Verify = async ({ token }) => {
  return JWT.verify(token, JWT_SECRET_KEY);
};

const tagCalculationFormula = (
  currentScore,
  questionRating,
  childsAnswer,
  target,
  is_tag,
  is_assessment = false
) => {
  let cVal = is_tag ? 30 : is_assessment ? 100 : 1;
  let kVal = is_assessment ? 104.8 : 166.1;
  let targetRating = target ? target : 0.7; // If We Do Target Is === 0.7
  childsAnswer = is_tag ? childsAnswer : !childsAnswer;
  targetRating = is_tag ? targetRating : 1 - targetRating;
  if (childsAnswer) {
    let newTagScore =
      currentScore +
      cVal *
        (1 -
          1 /
            (1 +
              (1 / targetRating - 1) *
                Math.pow(10, (questionRating - currentScore) / kVal)));
    return newTagScore < 0 ? 5 : newTagScore.toFixed(2);
  } else {
    let newTagScore =
      currentScore +
      cVal *
        (0 -
          1 /
            (1 +
              (1 / targetRating - 1) *
                Math.pow(10, (questionRating - currentScore) / kVal)));
    return newTagScore < 0 ? 5 : newTagScore.toFixed(2);
  }
};

const hashPassword = async (password) => {
  // return bcrypt.hashSync(password, 10);
  // trying this for sake of existing accounts
  return bcrypt.hashSync(password, 6);
};

const isPasswordMatch = async (password, hashedPassword) => {
  const isPasswordMatch = bcrypt.compareSync(password, hashedPassword);
  return isPasswordMatch;
};

const getGradeID = (name) => {
  const values = ["Pre-K", "Kindergarten", "First", "Second", "Third"];
  return name === "Pre-K" ? "0" : values.indexOf(name) - 1 + "";
};

const CSVParser = async (TYPE) => {
  let path = "";
  let fileName = "";
  switch (TYPE) {
    case LESSON:
      path = "data/lessons.csv";
      fileName = "data/lessons.json";
      break;
    case QUESTION:
      path = "data/questions.csv";
      fileName = "data/questions.json";
      break;
    case TAG:
      path = "data/tags.csv";
      fileName = "data/tags.json";
      break;
    case RELATION:
      path = "data/relations.csv";
      fileName = "data/relations.json";
      break;
    case RATINGS:
      path = "data/ratings.csv";
      break;
    case PARENTS:
      path = "data/parents.csv";
      fs.createReadStream(path)
        .on("error", (err) => console.log("Error Occurred ", err))
        .pipe(parser())
        .on("data", async (data) => {
          try {
            const insertParent = await Parent.create(data);
          } catch (err) {
            console.log("Error inserting parent: ", err);
          }
        });
      return;
    case CHILDREN:
      path = "data/children.csv";
      break;
    case DOMAIN_GRADE:
      path = "data/domaingrades.csv";
      break;
    case DOMAIN:
      path = "data/domains.csv";
    case LESSONQUESTION:
      path = "data/lessonQuestion.csv";
      break;
    case QUESTIONIDUPDATE:
      path = "data/questionIdUpdate.csv";
      break;
    case ANSWER_RESULT:
      path = "data/answerResults.csv";
      break;
    case PERFORMANCE:
      path = "data/tagPerf.csv";
      break;
    case DATE:
      path = "data/date_ref.csv";
      break;
    case COUNTING_CARD:
      path = "data/countingCard.csv";
      break;
    case NUMBERS_BASE:
      path = "data/numbersBase.csv";
      break;
    case OPERATIONS_ALGEBRAIC:
      path = "data/operations.csv";
      break;
    case MEASURE_DATA:
      path = "data/measureData.csv";
      break;
    case GEOMETRY:
      path = "data/geometry.csv";
      break;
    case NUMBERS_FRACTIONS:
      path = "data/numbersFractions.csv";
      break;
    case ASSESSMENT:
      path = "data/assessment.csv";
      break;
    default:
      break;
  }
  const array = [];
  fs.createReadStream(path)
    .on("error", (err) => console.log("Error Occurred ", err))
    .pipe(parser())
    .on("data", (data) => array.push(data))
    .on("end", async () => {
      // fs.writeFileSync(fileName, JSON.stringify(array));
      switch (TYPE) {
        case LESSON:
          const lessons = await Lesson.bulkCreate(array);
          break;
        case QUESTION:
          try {
            const questions = await Question.bulkCreate(array);
          } catch (e) {
            console.log("error bulk questions: ", e);
          }
          break;
        case TAG:
          const tags = await Tag.bulkCreate(array);
          break;
        case RELATION:
          const relations = await TagLesson.bulkCreate(array);
          break;
        case CHILDREN:
          const children = await Child.bulkCreate(array);
          break;
        case RATINGS:
          const ratings = await TagRating.bulkCreate(array);
          break;
        case DOMAIN_GRADE:
          const domainGrades = await DomainGrade.bulkCreate(array);
          break;
        case DOMAIN:
          const domains = await Domain.bulkCreate(array);
          break;
        case LESSONQUESTION:
          try {
            const lessonQuestions = await LessonQuestion.bulkCreate(array);
            break;
          } catch (e) {
            console.log("lessonQuestions error: ", e);
            break;
          }
        case QUESTIONIDUPDATE:
          const questionUpdate = await QuestionIdUpdate.bulkCreate(array);
          break;
        case ANSWER_RESULT:
          try {
            const answer = await Answer.bulkCreate(array);
            break;
          } catch (e) {
            console.log("answer result bulkCreate error: ", e);
            break;
          }
        case PERFORMANCE:
          try {
            const perf = TagPerformance.bulkCreate(array);
            break;
          } catch (e) {
            console.log("bulkCreate tagPerf error: ", e);
            break;
          }
        case DATE:
          try {
            // const dates = DateRef.bulkCreate(array);
            break;
          } catch (e) {
            console.log("error bulk insert date: ", e);
            break;
          }
        case COUNTING_CARD:
          try {
            const countingCard = await CountingCard.bulkCreate(array);
            break;
          } catch (e) {
            console.log("error bulk insert counting card");
            break;
          }
        case OPERATIONS_ALGEBRAIC:
          try {
            const operations = await OperationAlgebraic.bulkCreate(array);
            break;
          } catch (e) {
            console.log("error bulk insert operations alge");
            break;
          }
        case NUMBERS_BASE:
          try {
            const numberBase = await NumbersBase.bulkCreate(array);
            break;
          } catch (e) {
            console.log("error bulk insert numbers base");
            break;
          }
        case MEASURE_DATA:
          try {
            const measureData = await MeasureData.bulkCreate(array);
            break;
          } catch (e) {
            console.log("error bulk insert measure data");
            break;
          }
        case GEOMETRY:
          try {
            const geo = await Geometry.bulkCreate(array);
            break;
          } catch (e) {
            console.log("error bulk insert geometry");
            break;
          }
        case NUMBERS_FRACTIONS:
          try {
            const numFrac = await NumbersFractions.bulkCreate(array);
            break;
          } catch (e) {
            console.log("error bulk insert num fracx", e);
            break;
          }
        case ASSESSMENT:
          try {
            const ass = await Assessment.bulkCreate(array);
            break;
          } catch (e) {
            console.log("error bulk create assessment", e);
            break;
          }
        default:
          break;
      }
    });
};

const getQuestionTags = (question) => {
  const { p_tag, s_tag1, s_tag2, s_tag3, s_tag4, s_tag5, s_tag6 } = question;
  let tagIDs = [];
  p_tag && tagIDs.push(p_tag);
  s_tag1 && tagIDs.push(s_tag1);
  s_tag2 && tagIDs.push(s_tag2);
  s_tag3 && tagIDs.push(s_tag3);
  s_tag4 && tagIDs.push(s_tag4);
  s_tag5 && tagIDs.push(s_tag5);
  s_tag6 && tagIDs.push(s_tag6);
  return tagIDs;
};

const createRandomPassword = () => {
  return crypto.randomBytes(10).toString("hex");
};

module.exports = {
  sendResponse,
  CSVParser,
  isPasswordMatch,
  hashPassword,
  tagCalculationFormula,
  JWT_SignIn,
  JWT_Verify,
  getGradeID,
  getQuestionTags,
  createRandomPassword,
};
