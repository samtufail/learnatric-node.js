module.exports = {
  subtractFromHighestAndDivide: (all, high, low) => {
    // console.log('lows: ', lows, 'high: ', high);
    const priorityArray = [];
    const highMinusLow = high.tag_rating - low.tag_rating;
    let sum = 0;
    all.forEach((rating) => {
      const { tag_id, tag_rating } = rating;
      // subtract current rating in the loop from the highest rating
      const variationFromHigh = Number.parseFloat(
        (high.tag_rating - tag_rating).toFixed(2)
      );
      const priority_level = Number.parseFloat(
        (variationFromHigh / highMinusLow).toFixed(2)
      );
      sum += priority_level;
      // push an obj to priority array with the low tag_id and the difference between high and low rating
      priorityArray.push({
        tag_id,
        current_rating: tag_rating,
        priority_level,
      });
    });

    // let sum = 0;
    // add all variations together
    // priorityArray.forEach((priority) => {
    //   sum += priority.variation_from_highest;
    // });
    const priorityMap = {};
    priorityArray.forEach((tag) => {
      tag.priority_weight = Number.parseFloat(
        ((tag.priority_level / sum) * 100).toFixed(2)
      );
      tag.checked = false;
      priorityMap[tag.tag_id] = tag;
    });
    // console.log('priorityArray divided into percents: ', priorityMap)
    return priorityMap;
  },

  createArrayOfLessonIds: (lessons) => {
    const result = [];

    lessons.forEach((lesson) => {
      result.push(lesson.dataValues.id);
    });
    // console.log('result array: ', result)
    return result;
  },

  createLessonsWithTagFocusObj: (relations) => {
    const lessonFocusMap = {};

    relations.forEach((relation) => {
      const priorityLevel = relation.priority === 1 ? 30 : 15;
      if (!lessonFocusMap[relation.lesson_id]) {
        lessonFocusMap[relation.lesson_id] = {};
        lessonFocusMap[relation.lesson_id][relation.tag_id] = {
          lesson_id: relation.lesson_id,
          tag_id: relation.tag_id,
          weight: priorityLevel,
        };
      } else {
        lessonFocusMap[relation.lesson_id][relation.tag_id] = {
          lesson_id: relation.lesson_id,
          tag_id: relation.tag_id,
          weight: priorityLevel,
        };
      }
    });

    // console.log('lessonsFocusMap: ', lessonFocusMap)
    return lessonFocusMap;
  },

  createTagsVariance: (tags, lessonsFocus) => {
    const lessonsWithVariance = {};

    const lessonFocusArray = Object.values(lessonsFocus);
    const tagsArray = Object.values(tags);

    lessonFocusArray.forEach((lesson, idx) => {
      let sum = 0;

      tagsArray.forEach((tag) => {
        if (lesson[tag.tag_id]) {
          if (tag.priority_weight > lesson[tag.tag_id].weight) {
            const diff = tag.priority_weight - lesson[tag.tag_id].weight;
            sum += diff;
          } else {
            sum += 0;
          }
          // let diff = lesson[tag.tag_id].weight - tag.priority_weight;
          // sum += diff < 0 ? Math.abs(diff) : diff;
        } else {
          sum += tag.priority_weight;
        }
      });
      const { lesson_id } = Object.values(lesson)[0];
      lessonsWithVariance[lesson_id] = {
        lesson_id,
        total_variance: sum,
      };
    });

    return lessonsWithVariance;
  },

  updateVariance: (totalVariancesMap, completedLessons) => {
    completedLessons.forEach((lesson) => {
      if (totalVariancesMap[lesson.lesson_id.toString()]) {
        // comment for now for testing
        // let lessonToUpdate = totalVariancesMap[lesson.lesson_id.toString()];
        // let newVariance = lessonToUpdate.total_variance + 25;
        // totalVariancesMap[lesson.lesson_id.toString()] = {
        //   lesson_id: lessonToUpdate.lesson_id,
        //   total_variance: newVariance,
        // };

        // this is only temp until they figure out a better way to curate a lesson
        delete totalVariancesMap[lesson.lesson_id.toString()];
      }
    });
    // console.log('updated variences? ', totalVariancesMap)
  },

  sortLessonsByVariance: (lessons) => {
    const array = Object.values(lessons);
    // console.log('lessons before sort: ', array)
    array.sort((a, b) => a.total_variance - b.total_variance);
    // console.log('lessons after sorted!!!!!! ', array)
    return array;
  },

  filterAndPickNextLesson: (completed, allLessons) => {
    const completedMap = new Map();

    completed.forEach((lesson) => {
      completedMap.set(lesson.dataValues.lesson_id, true);
    });
    // console.log('map: ', completedMap)
    for (let i = 0; i < allLessons.length; i++) {
      if (!completedMap.has(allLessons[i].dataValues.id)) {
        // console.log('id to fetch: ', allLessons[i].dataValues.id)
        return allLessons[i].dataValues.id;
      }
    }
  },
};
