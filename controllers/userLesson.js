const { UserLesson, Child } = require("../models");
const { sendResponse } = require("../Utils");

const addUserLesson = async (req, res, next) => {
  const { child_id, lesson_id, user_lesson_id } = req.body;
  // console.log('req body: ', req.body);
  // this is for if the student logged out or left the computer without finishing a lesson then we will continue to update the same record for that user_lesson_id
  if (user_lesson_id) {
    let updateUserLesson = await UserLesson.findOne({
      where: { user_lesson_id: user_lesson_id },
      raw: true,
    });
    // console.log('there was a user_lesson_id: ', updateUserLesson)
    return sendResponse(
      res,
      "success",
      200,
      "User Lesson Details",
      updateUserLesson
    );
  }

  let userLesson = await UserLesson.findAll({
    where: { child_id: child_id, lesson_id: lesson_id },
    order: [["user_lesson_id", "ASC"]],
    raw: true,
  });
  // console.log('userLesson', userLesson)
  if (userLesson.length) {
    const incrementAttempts = {
      child_id,
      lesson_id,
      times_attempted: userLesson[userLesson.length - 1].times_attempted + 1,
    };
    const updateUserLesson = await UserLesson.create(incrementAttempts);
    // console.log('updateUserLesson length: ', updateUserLesson)
    const updatedChild = await Child.update(
      {
        lesson_id: lesson_id,
        user_lesson_id: updateUserLesson.dataValues.user_lesson_id,
      },
      {
        where: { id: child_id },
        returning: true,
        plain: true,
        raw: true,
      }
    );
    // console.log('updated child length: ', updatedChild)
    const returnData = { updateUserLesson, updatedChild };
    return sendResponse(res, "success", 200, "User Lesson Details", returnData);
  } else {
    userLesson = {
      child_id,
      lesson_id,
      times_attempted: 1,
    };
    const updateUserLesson = await UserLesson.create(userLesson);
    // console.log('updateUserLesson no length: ', updateUserLesson)
    const updatedChild = await Child.update(
      {
        lesson_id: lesson_id,
        user_lesson_id: updateUserLesson.dataValues.user_lesson_id,
      },
      {
        where: { id: child_id },
        returning: true,
        plain: true,
        raw: true,
      }
    );
    // console.log('updated child no length: ', updatedChild)
    const returnData = { updateUserLesson, updatedChild };
    return sendResponse(res, "success", 200, "User Lesson Details", returnData);
  }
};

const updateUserLesson = async (req, res, next) => {
  const { userLessonID } = req.params;
  // console.log("query: ", userLessonID);
  const { field, child_id } = req.body;
  // console.log('child_id: ', req.body)
  // console.log('field: ', field);
  if (field === "start-over") {
    const update = await Child.update(
      { stepInLesson: null },
      { where: { id: child_id }, returning: true, plain: true, raw: true }
    );
    const response = await UserLesson.update(
      { instruction_completed: false },
      { where: { user_lesson_id: userLessonID }, returning: true, plain: true }
    );
    const returnData = { update, response };
    return sendResponse(
      res,
      "success",
      200,
      "User Lesson Updated Successfully",
      returnData
    );
  } else {
    let update;
    if (field === "instruction_completed") {
      update = await Child.update(
        { stepInLesson: "we_do" },
        { where: { id: child_id }, returning: true, plain: true, raw: true }
      );
      // console.log('instruction_completed child update: ', update)
    } else if (field === "activity_completed") {
      update = await Child.update(
        { stepInLesson: "question_set", questionIndex: 0 },
        { where: { id: child_id }, returning: true, plain: true, raw: true }
      );
      // console.log('update after activity completed: ', update)
    } else if (field === "problem_set") {
      console.log("body on prblem_set: ", req.body);
    }
    const response = await UserLesson.update(
      { [field]: true },
      { where: { user_lesson_id: userLessonID }, returning: true, plain: true }
    );
    const returnData = { update, response };
    return sendResponse(
      res,
      "success",
      200,
      "User Lesson Updated Successfully",
      returnData
    );
  }
};

const updateTrainWobble = async (req, res) => {
  const { wobble, trainNumber, child_id } = req.body;

  console.log("body: ", req.body);

  let update;

  if (wobble === 0 && trainNumber === 1) {
    // if train made it to castle, update both
    console.log("reset!!!!!");
    update = await Child.update(
      { wobble, trainNumber },
      { where: { id: child_id }, returning: true, plain: true }
    );
    return sendResponse(
      res,
      "success",
      200,
      "Updated trainNumber and wobble",
      update
    );
  } else if (wobble !== null && trainNumber === null) {
    //just update wobble
    update = await Child.update(
      { wobble },
      { where: { id: child_id }, returning: true, plain: true }
    );
    return sendResponse(
      res,
      "success",
      200,
      "Updated trainNumber and wobble",
      update
    );
  } else if (wobble === null && trainNumber !== null) {
    // just update trainNumber
    update = await Child.update(
      { trainNumber },
      { where: { id: child_id }, returning: true, plain: true }
    );
    return sendResponse(
      res,
      "success",
      200,
      "Updated trainNumber and wobble",
      update
    );
  }
};

module.exports = {
  addUserLesson,
  updateUserLesson,
  updateTrainWobble,
};
