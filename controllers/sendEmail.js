const mailgun = require("mailgun-js");
const moment = require("moment");
const { sendResponse } = require("../Utils");
const { MAIL_GUN_API_KEY } = require('../constants');
const DOMAIN = 'learnatric.com';

const SignupEmail = async (req, res) => {
  const { email, firstName } = req.body

  const mg = mailgun({apiKey: MAIL_GUN_API_KEY, domain: DOMAIN});
  try {
    const data = {
      from: "Learnatric <ben@learnatric.com>",
      to: `${email}`,
      subject: "Thank You",
      template: "thanks",
      'v:Name': firstName,
      'v:trail_start': `${moment().format('MMM Do YY')}`,
      'v:trail_end': `${moment().add(30, 'days').format('MMM Do YY')}`
    };
    mg.messages().send(data, function (error, body) {
      if (error) {
        return sendResponse(res, 'fail', 401, error)
      }
      return sendResponse(res, 'success', 202, body.message)
    });
  } catch (e) {
    console.log('error sending initial thank you email', e)
    return sendResponse(res, 'fail', 401, 'Error sending email')
  }
}

const ForgotPasswordEmail = async (email, name, password) => {
  const mg = mailgun({apiKey: MAIL_GUN_API_KEY, domain: DOMAIN});
  try {
    const data = {
      from: "Learnatric <ben@learnatric.com>",
      to: `${email}`,
      subject: "Password Change",
      template: "password_change",
      'v:name': name,
      'v:password': password
    };
    const message = await mg.messages().send(data)
    return message
    // , function (error, body) {
    //   console.log(body);
    //   if (error) {
    //     return 'error sending email'
    //   }
    //   return body.message
    // });
  } catch (e) {
    console.log('error sending password change email', e)
    return sendResponse(res, 'fail', 401, 'Error sending email')
  }
}

module.exports = {
  SignupEmail,
  ForgotPasswordEmail
}