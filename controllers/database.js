const { Tag, Lesson, Question, TagLesson, connection } = require("../models");

// const tagsJSON = require("../data/tags.json");
// const questionsJSON = require("../data/questions.json");
// const lessonsJSON = require("../data/lessons.json");
// const relationsJSON = require("../data/relations.json");

const { sendResponse, CSVParser } = require("../Utils");
// const { RELATION, LESSON, TAG, QUESTION } = require("../constants");

const SyncDatabase = async (req, res, next) => {
  try {
    await connection.sync({ alter: true });
    return sendResponse(res, "success", 200, "Database Synced", null);
  } catch (err) {
    console.log('err sync DB: ', err);
  }
};

const ForceSyncDatabase = async (req, res, next) => {
  // await CSVParser(LESSON);
  // await CSVParser(TAG);
  // await CSVParser(QUESTION);
  // await CSVParser(RELATION);
  await connection.sync({ force: true });
  // await Question.bulkCreate(questionsJSON);
  // await Lesson.bulkCreate(lessonsJSON);
  // await Tags.bulkCreate(tagsJSON);
  // await T_L_Relationship.bulkCreate(relationsJSON);
  return sendResponse(res, "success", 200, "Database Forced Synced", null);
};

const BulkCreate = async (req, res, next) => {
  const { type } = req.query;
  try {
    await CSVParser(type);
    // const relations = await TagLesson.bulkCreate(relationsJSON)
    return sendResponse(res, "success", 200, `Database Bulk Create ${type}`, null);
  } catch (e) {
    console.log('error bulk creating relations: ', e);
    return sendResponse(res, "fail", 500, `Database Bulk Create ${type} error`, e);
  }
}

const tester = async (req, res, next) => {
  // await Question.bulkCreate(questionsJSON);
  // await Lesson.bulkCreate(lessonsJSON);
  // await Tag.bulkCreate(tagsJSON);
  // await TagLesson.bulkCreate(relationsJSON);
  // return sendResponse(res, "success", 200, "Test API", null);
};

module.exports = {
  SyncDatabase,
  ForceSyncDatabase,
  tester,
  BulkCreate
};
