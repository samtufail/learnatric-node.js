const { Tag } = require("../models");
// const tagsJSON = require("../data/tags.json");

const addTagsFromJSON = async (req, res, next) => {
  await Tag.bulkCreate(tagsJSON);
  return sendResponse(res, "success", 200, "Tags Successfully Added To Database From JSON", null);
};

module.exports = {
  addTagsFromJSON,
};
