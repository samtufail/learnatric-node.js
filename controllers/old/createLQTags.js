const { LQ } = require("../services/lq");
const { TagsData } = require("../services/Tag");
const qCSV = require("../data/questions.json");
const lessonJSON = require("../data/lessons.json");
const tagsJSON = require("../data/tags.json");
const httpStatus = require("http-status");
const APIResponse = require("../Utils/APIResponse");
const { AnswerResult } = require("../services/Answer");

// const { parse } = require('../Utils/csvParser')
// const csvParser = require('csv-parser');

module.exports = {
  createLesson: async (req, res, next) => {
    try {
      let adder = new LQ();
      const newLesson = await adder.addLesson(req.body);
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(newLesson, "Create lesson success..!!", httpStatus.OK)
        );
    } catch (error) {
      next(error);
    }
  },
  createQuestion: async (req, res, next) => {
    try {
      let adder = new LQ();
      const newQuestion = await adder.addQuestion(req.body);
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            newQuestion,
            "create question success..!!",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },

  getLessonQuestionData: async (req, res, next) => {
    try {
      const lesson = new LQ();
      let questionLessonData = await lesson.getLessonQuestionData(req.query.id);

      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            questionLessonData,
            "Fetch Lesson Question Data Success..!!",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },

  bulkCreateQuestion: async (req, res, next) => {
    try {
      const bulkQuestions = new LQ();
      const createBulkQuestions = await bulkQuestions.bulkCreateQuestion(qCSV);
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            createBulkQuestions,
            "Bulk create question success..!!",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },

  bulkCreateLesson: async (req, res, next) => {
    try {
      const bulkLessons = new LQ();
      const createBulkLessons = await bulkLessons.bulkCreateLesson(lessonJSON);
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            createBulkLessons,
            "Bulk create lesson success..!!",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },

  bulkCreateTags: async (req, res, next) => {
    try {
      const bulkTags = new TagsData();
      const createBulkTags = await bulkTags.bulkInsertTags(tagsJSON);
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            createBulkTags,
            "Bulk create tag success..!!",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },

  answerQuestion: async (req, res, next) => {
    try {
      const answer = new AnswerResult();
      const data = req.body.data;
      const createAnswer = await answer.answerResult(data);
      res.json(createAnswer);
    } catch (error) {
      next(error);
    }
  },
};
