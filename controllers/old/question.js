const { FilterQuestion } = require("../services/Question");
const httpStatus = require("http-status");
const APIResponse = require("../Utils/APIResponse");
const { Question } = require("../database");

module.exports = {
  filterQuestion: async (req, res, next) => {
    try {
      const question = new FilterQuestion();
      const allQuestion = await question.filterQuestion(req.query.lesson_id);
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            allQuestion,
            "Filter Question Fetch Successfully..!",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },
};
