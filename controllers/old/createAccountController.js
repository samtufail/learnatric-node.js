const { NewAccount } = require("../services/createAccount");
const { TagsData } = require("../services/Tag");
const { TagsRatingData } = require("../services/tagPerformance");
const { StripeModal } = require("../stripe/");
const { connection } = require("../database/index");
const httpStatus = require("http-status");
const APIResponse = require("../Utils/APIResponse");

module.exports = {
  migrate: async (req, res, next) => {
    try {
      await connection.sync({ alter: true });
      // only use if you want to drop tables and re-create
      // await connection.sync({ force: true})
      return res.sendStatus(200);
    } catch (error) {
      next(error);
    }
  },

  newParent: async (req, res, next) => {
    try {
      const parent = new NewAccount();
      const {
        FirstName,
        LastName,
        Email,
        password,
        hear_about_us,
        child_count,
        stripe_customer_id,
        stripe_sub_id,
      } = req.body;
      const createdParent = await parent.addParent({
        FirstName: FirstName,
        LastName: LastName,
        Email: Email,
        password: password,
        hear_about_us: hear_about_us,
        child_count: child_count,
        stripe_customer_id: stripe_customer_id,
        stripe_sub_id: stripe_sub_id,
      });
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            createdParent,
            "Parent create succsee..!!",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },

  newChild: async (req, res, next) => {
    try {
      const child = new NewAccount();
      const {
        firstName,
        lastName,
        birthday,
        gender,
        grade,
        school,
        teachersName,
        teachersEmail,
        schoolDistrict,
        isOkayToEmailTeacher,
        parentId,
      } = req.body;
      let createdChild = await child.addChild({
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        gender: gender,
        grade: grade,
        school: school,
        teachersName: teachersName,
        teachersEmail: teachersEmail,
        schoolDistrict: schoolDistrict,
        isOkayToEmailTeacher: isOkayToEmailTeacher,
        parentId: parentId,
        isFirstLogin: true,
      });

      const childGrade = createdChild.dataValues.grade;
      const startVal =
        childGrade === "Pre-K"
          ? 20
          : childGrade === "Kindergarten"
          ? 50
          : childGrade === "First"
          ? 100
          : childGrade === "Second"
          ? 200
          : 300;

      //Fetch All Tag Data
      let tags = new TagsData();
      let allTags = await tags.getTags();

      // let records = allTags.map((tag) => {
      //     let _tag = tag.toJSON();
      //     return {
      //         child_id: createdChild.id,
      //         tag_id: _tag.tag_id,
      //         tag_rating: startVal || null,
      //         tag_rating_start: startVal || null,
      //     };
      // });

      //Tag Rating bulk create when new child create
      let datas = allTags.map((tags) => {
        let __tag = tags.toJSON();
        return {
          child_id: createdChild.id,
          tag_id: __tag.tag_id,
          tag_rating: startVal || null,
          // tag_rating_max: __tag.tag_rating_max || null,
          // tag_rating_start: startVal || null,
        };
      });

      //Insert Data Tag_perfomance collection
      // const results = await tags.bulkInsertTagPerfomance(records);
      const results = await tags.bulkInsertTagRating(datas);

      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            createdChild,
            "Child create success..!!",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },

  subscription: async (req, res, next) => {
    try {
      const { email, payment_method, child_count, plan_selected } = req.body;
      const stripe = new StripeModal();
      const customer = await stripe.createCusomter(email, payment_method);
      const subscription = await stripe.createSubscription(
        customer.id,
        plan_selected,
        child_count
      );
      const status = subscription.latest_invoice.status;
      res.json({
        status: status,
        stripe_customer_id: customer.id,
        stripe_sub_id: subscription.id,
      });
    } catch (error) {
      console.log("Error Came");
      next(error);
    }
  },

  fetchChildren: async (req, res, next) => {
    try {
      const children = new NewAccount();
      const allChildren = await children.findAllChildren(req.query.parentId);
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            allChildren,
            "allchildren came back from db",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },

  createChildProfile: async (req, res, next) => {
    try {
      const childProfile = new NewAccount();
      // const { username, password, avatar_small, avatar_large, child_id, isLastChild } = req.body;
      const promises = [];
      req.body.forEach((child) => {
        promises.push(
          childProfile.createChildProfile(
            child.username,
            child.password,
            child.avatar_small,
            child.avatar_large,
            child.child_id
          )
        );
      });
      const createdChildProfiles = await Promise.allSettled(promises);
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            createdChildProfiles,
            "Child profile create success..!!",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },

  passLesson: async (req, res, next) => {
    try {
      const children = new NewAccount();
      const updatedChildrenProfile = await children.updateChildLesson(
        req.body.id
      );
      return res
        .status(httpStatus.OK)
        .json(new APIResponse(updatedChildrenProfile[0], " ", httpStatus.OK));
    } catch (error) {
      next(error);
    }
  },

  getChildDetail: async (req, res, next) => {
    try {
      const children = new NewAccount();
      const childDetail = await children.getChildDetail(req.query.id);
      return res
        .status(httpStatus.OK)
        .json(new APIResponse(childDetail, "Child Detail..", httpStatus.OK));
    } catch (error) {
      next(error);
    }
  },
};
