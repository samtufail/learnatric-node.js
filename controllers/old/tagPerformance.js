const { TagsRatingData } = require("../services/tagPerformance");
const { UserLesson } = require("../services/UserLesson");
const { AnswerResult } = require("../services/Answer");
const httpStatus = require("http-status");
const APIResponse = require("../Utils/APIResponse");

module.exports = {
  getTagPerformanceData: async (req, res, next) => {
    try {
      const tagPerfomance = new TagsRatingData();
      const allTags = await tagPerfomance.getTagPerformance(req.query.child_id);
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            allTags,
            "Fetch Tag Perfomance Data..!!",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },

  updateTagRating: async (req, res, next) => {
    try {
      const tagPerfomance = new TagsRatingData();
      const answerResult = new AnswerResult();
      const createPromiseArray = (data) => {
        const numbers = [];
        const promises = [];
        data.s_tags.forEach((tag) => {
          if (Number.parseInt(tag)) {
            numbers.push(Number.parseInt(tag));
          }
        });
        if (numbers.length) {
          promises.push(
            tagPerfomance.updateTagRating({
              child_id: data.child_id,
              isCorrect: data.isCorrect,
              tag_id: Number.parseInt(data.p_tag),
              question_rating: data.question_rating,
              lesson_id: data.lesson_id,
              question_id: data.question_id,
              is_we_do: false,
              target: Number.parseFloat(data.target),
            })
          );
          numbers.forEach((s_tag) => {
            promises.push(
              tagPerfomance.updateTagRating({
                child_id: data.child_id,
                isCorrect: data.isCorrect,
                tag_id: s_tag,
                question_rating: data.question_rating,
                lesson_id: data.lesson_id,
                question_id: data.question_id,
                is_we_do: false,
                target: Number.parseFloat(data.target),
              })
            );
          });
          return promises;
        } else {
          promises.push(
            tagPerfomance.updateTagRating({
              child_id: data.child_id,
              isCorrect: data.isCorrect,
              tag_id: Number.parseInt(data.p_tag),
              question_rating: data.question_rating,
              lesson_id: data.lesson_id,
              question_id: data.question_id,
              is_we_do: false,
              target: Number.parseFloat(data.target),
            })
          );
          return promises;
        }
      };
      if (
        req.body.is_last_question === true ||
        req.body.is_last_question === "true"
      ) {
        try {
          const userLessons = new UserLesson();
          let updateAnswerResult = await answerResult.addAnswerResult(req.body);
          const arrayToUpdate = await createPromiseArray(req.body);
          const updateLastQuestion = Promise.all(arrayToUpdate);

          const gradeLevel =
            req.body.grade === "Third"
              ? 3
              : req.body.grade === "Second"
              ? 2
              : req.body.grade === "First"
              ? 1
              : 0;
          const filtered = await userLessons.filterLessonsNotCompleted(
            gradeLevel.toString(),
            req.body.child_id
          );

          return res
            .status(httpStatus.OK)
            .json(
              new APIResponse(
                filtered,
                "Fetch Tag Perfomance Data..!!",
                httpStatus.OK
              )
            );
        } catch (e) {
          return res
            .status(httpStatus.INTERNAL_SERVER_ERROR)
            .json(
              new APIResponse(
                {},
                "error querying DB for Update Tag Perfomance",
                httpStatus.INTERNAL_SERVER_ERROR
              )
            );
        }
      } else if (req.body.is_we_do === true || req.body.is_we_do === "true") {
        try {
          let updateAnswerResult = await answerResult.addAnswerResult(req.body);
          const weDoUpdate = await tagPerfomance.updateTagRating({
            ...req.body,
            is_we_do: true,
          });
          res.json({ data: "API updated your tags for a we do question" });
        } catch (e) {
          return res
            .status(httpStatus.INTERNAL_SERVER_ERROR)
            .json(
              new APIResponse(
                {},
                "error querying DB for Update Tag Perfomance",
                httpStatus.INTERNAL_SERVER_ERROR
              )
            );
        }
      } else {
        let updateAnswerResult = await answerResult.addAnswerResult(req.body);
        const wait = await createPromiseArray(req.body);
        const everythingUpdated = await Promise.all(wait);
        res.json({ data: "API updated your tags for a regular question" });
      }
    } catch (error) {
      next(error);
    }
  },
};
