const { AnswerResult } = require("../services/Answer");
const httpStatus = require("http-status");
const APIResponse = require("../Utils/APIResponse");

module.exports = {
  createAnswerResult: async (req, res, next) => {
    try {
      const answerResult = new AnswerResult();
      const { user_id, lesson_id, question_id, text_answer, is_correct, date_time } = req.body;

      const answerresult = await answerResult.answerResult({
        user_id: user_id,
        lesson_id: lesson_id,
        question_id: question_id,
        text_answer: text_answer,
        is_correct: is_correct,
        date_time: date_time,
      });

      return res
        .status(httpStatus.OK)
        .json(new APIResponse(answerresult, "Answer Result Data Inserted Successfully", httpStatus.OK));
    } catch (error) {
      next(error);
    }
  },

  fetchAnswerResult: async (req, res, next) => {
    try {
      const answerResult = new AnswerResult();
      const allAnswerResult = await answerResult.findAllAnswerResult(req.query.answer_result_id);
      return res.status(httpStatus.OK).json(new APIResponse(allAnswerResult, "Answer Result", httpStatus.OK));
    } catch (error) {
      next(error);
    }
  },

  deleteAnswerResult: async (req, res, next) => {
    try {
      const answerResult = new AnswerResult();
      const deleteAnswerresult = await answerResult.deleteAnswerResult(req.query.answer_result_id);
      return res.status(httpStatus.OK).json(new APIResponse(deleteAnswerresult, "Delete success..!!", httpStatus.OK));
    } catch (error) {
      next(error);
    }
  },

  updateAnswerResult: async (req, res, next) => {
    try {
      const answerResult = new AnswerResult();
      const updateAnswerResult = await answerResult.updateAnswerResult(req.body, req.query.answer_result_id);

      return res
        .status(httpStatus.OK)
        .json(new APIResponse(updateAnswerResult, "Answer Result Data Inserted Successfully", httpStatus.OK));
    } catch (error) {
      next(error);
    }
  },

  getLessonQuestionData: async (req, res, next) => {
    try {
      const lessonQuestion = new AnswerResult();
      let lessonQuestionData = await lessonQuestion.getLessonQuestion(req.query.answer_result_id);

      return res
        .status(httpStatus.OK)
        .json(new APIResponse(lessonQuestionData, "Lesson Question Data Fetched successfully", httpStatus.OK));
    } catch (error) {
      next(error);
    }
  },
};
