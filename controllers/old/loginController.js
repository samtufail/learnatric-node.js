const { PasswordAuth } = require("../services/PasswordAuth");
const { jwtSign } = require("../Utils/JWTAuthenticator");
const httpStatus = require("http-status");
const APIResponse = require("../Utils/APIResponse");

module.exports = {
  loginController: async (req, res, next) => {
    try {
      const { username, password } = req.body;
      const auth = new PasswordAuth();
      const checkForAccount = await auth.findAccount(username, password);
      let response;
      if (typeof checkForAccount !== "string") {
        const { account, type } = checkForAccount;
        if (type == "parent") {
          const { Email, id } = account;
          checkForAccount.account.token = jwtSign({ Email, id, type });
        } else {
          const { username, id } = checkForAccount;

          checkForAccount.account.token = jwtSign({ username, id, type });
        }
        response = new APIResponse(checkForAccount, "Login successfully", 200);
      } else {
        response = new APIResponse(null, checkForAccount, 404);
      }
      return res.send(response);
    } catch (err) {
      next(error);
    }
  },
};
