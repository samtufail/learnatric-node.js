const { UserLesson } = require("../services/UserLesson");
const httpStatus = require("http-status");
const APIResponse = require("../Utils/APIResponse");

module.exports = {
  createUserLesson: async (req, res, next) => {
    try {
      const userlesson = new UserLesson();
      const {
        child_id,
        lesson_id,
        instruction_completed,
        activity_completed,
        problem_set,
        start_date_time,
        end_date_time,
      } = req.body;
      const createUserLesson = await userlesson.userLesson({
        child_id: child_id,
        lesson_id: lesson_id,
        instruction_completed: instruction_completed,
        activity_completed: activity_completed,
        problem_set: problem_set,
        start_date_time: start_date_time,
        end_date_time: end_date_time,
      });
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            createUserLesson,
            "User Lesson Data Inserted Successfully",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },
};
