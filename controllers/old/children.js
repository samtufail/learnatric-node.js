const { ChildrenData } = require("../services/Children");
const httpStatus = require("http-status");
const APIResponse = require("../Utils/APIResponse");

module.exports = {
  getChildTagData: async (req, res, next) => {
    try {
      const children = new ChildrenData();
      let childData = await children.getChildsrenData(req.query.id);
      return res
        .status(httpStatus.OK)
        .json(
          new APIResponse(
            childData,
            "Children Data Fetched successfully",
            httpStatus.OK
          )
        );
    } catch (error) {
      next(error);
    }
  },
};
