const { Child, Parent, Tag, TagRating, TagPerformance } = require("../models");
const { sendResponse, hashPassword, getGradeID } = require("../Utils");
const moment = require("moment");
const addChild = async (req, res) => {
  const {
    firstName,
    lastName,
    dob,
    gender,
    grade,
    school,
    teacherName,
    teacherEmail,
    schoolDistrict,
    canEmailTeacher,
    parentId,
  } = req.body;

  const child = {
    firstName,
    lastName,
    dob,
    gender,
    grade,
    school,
    teacherName,
    teacherEmail,
    schoolDistrict,
    canEmailTeacher,
    parentId,
  };
  // console.log('child: ', child)

  let createdChild = await Child.create(child);
  await Parent.increment("noOfChildsProfileCompleted", {
    by: 1,
    where: { id: parentId },
  });

  const parent = await Parent.findOne({ where: { id: parentId } });
  if (parent.noOfChildsProfileCompleted === parent.childs) {
    await Parent.update({ step: 2 }, { where: { id: parentId } });
  }

  return sendResponse(
    res,
    "success",
    201,
    "Child Created Successfully",
    createdChild
  );
};

const getParentChilds = async (req, res) => {
  const { Id } = req.query;
  const childs = await Child.findAll({ where: { parentId: Id } });
  // console.log('childs: ', childs)
  const activeChildren = [];
  for (let child of childs) {
    if (child.dataValues.is_active_subscription) {
      activeChildren.push(child);
    }
  }
  return sendResponse(
    res,
    "success",
    200,
    "Fetched All the Childs Of A Parent",
    activeChildren
  );
};

const updateChild = async (req, res) => {
  // Getting the Data From Body

  let { username, password, avatarSmall, avatarLarge, childId, parentId } =
    req.body;

  const isUsernameAlreadyExist = await Child.findOne({
    where: { username: username },
  });
  if (isUsernameAlreadyExist) {
    return sendResponse(res, "fail", 409, "Username Already Exists");
  }
  password = await hashPassword(password);
  const updatedData = {
    avatarLarge,
    avatarSmall,
    username,
    password,
    isFirstLogin: true,
  };

  // Update the Child

  const child = await Child.update(updatedData, {
    where: { id: childId },
    returning: true,
    plain: true,
  });

  // Update the Completed Creds Count

  await Parent.increment("noOfChildsCredsCompleted", {
    by: 1,
    where: { id: parentId },
  });

  // If Step Completed Increase Counter For Step as well

  const parent = await Parent.findOne({ where: { id: parentId } });
  // console.log('parent: ', parent);
  if (parent.noOfChildsCredsCompleted === parent.childs) {
    await Parent.update({ step: 3 }, { where: { id: parentId } });
  }

  const childGrade = child[1].dataValues.grade;

  // const gradeID = getGradeID(childGrade);
  // console.log('gradeID: ', gradeID);

  const tags = await Tag.findAll();
  // console.log('tags: ', tags);
  // Create Tag Table

  const monthsArray = [9, 10, 11, 12, 1, 2, 3, 4, 5];
  let month = moment(new Date()).month() + 1;
  const start =
    childGrade === "Pre-K"
      ? 1
      : childGrade === "Kindergarten"
        ? 50
        : childGrade === "First"
          ? 100
          : childGrade === "Second"
            ? 200
            : 300;
  const gap =
    childGrade === "Pre-K" ? 50 : childGrade === "Kindergarten" ? 50 : 100;

  const tagStart =
    monthsArray.indexOf(month) === -1
      ? start
      : start + (monthsArray.indexOf(month) / 8) * gap;

  // const monthAddOn =
  //   month >= 6 && month <= 8
  //     ? 0
  //     : month >= 9 && month <= 11
  //     ? 20
  //     : month === 12 || (month >= 0 && month <= 2)
  //     ? 40
  //     : 60;
  // const startRating =
  //   childGrade === "Pre-K"
  //     ? 10 + monthAddOn
  //     : childGrade === "Kindergarten"
  //     ? 50 + monthAddOn
  //     : childGrade === "First"
  //     ? 100 + monthAddOn
  //     : childGrade === "Second"
  //     ? 200 + monthAddOn
  //     : 300 + monthAddOn;
  const tagsRatings = tags.map((item) => {
    return {
      child_id: child[1].dataValues.id,
      tag_id: item.dataValues.tag_id,
      tag_rating: tagStart,
      grade: item.dataValues.grade,
    };
  });

  // comment this for now while im buiding assessment b/c i think it should only be initated after completion of assessment with those start values
  // const tagPerformanceStart = tags.map((item) => {
  //   return {
  //     child_id: child[1].dataValues.id,
  //     question_id: 0,
  //     lesson_id: 0,
  //     tag_id: item.dataValues.tag_id,
  //     question_rating: startRating,
  //     tag_rating_start: startRating,
  //     is_correct: true,
  //     rating_new: startRating,
  //     is_we_do: false,
  //   };
  // });
  // // Save the Tag Data
  await TagRating.bulkCreate(tagsRatings);
  // await TagPerformance.bulkCreate(tagPerformanceStart);
  return sendResponse(res, "success", 200, "Child Updated Succesfully", child);
};

const setChildToys = async (req, res) => {
  // console.log("req body: ", req.body);
  const { toys, child_id } = req.body;
  const updateChild = await Child.update(
    {
      toys: JSON.stringify(toys),
    },
    {
      where: { id: child_id },
      returning: true,
      plain: true,
    }
  );
  // console.log("updated child: ", updateChild);
  return sendResponse(
    res,
    "success",
    200,
    "Child toys updated",
    updateChild[1].dataValues
  );
};

module.exports = {
  addChild,
  getParentChilds,
  updateChild,
  setChildToys,
};
