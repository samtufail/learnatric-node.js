const { Parent, Child } = require("../models");
const { ForgotPasswordEmail } = require("./sendEmail");
const { hashPassword, isPasswordMatch, sendResponse, createRandomPassword } = require("../Utils");

const UpdateParentPassword = async (req, res) => {
  const { id, current_pass, new_pass } = req.body;
  try {
    const parent = await Parent.findOne({
      where: { id: id },
      attributes: ["password"],
      raw: true
    });
    const passwordMatched = await isPasswordMatch(current_pass, parent.password);
    if (!passwordMatched) {
      return sendResponse(res, "fail", 401, "Your current password is incorrect, re-enter and try again");
    } else {
      const newPassword = await hashPassword(new_pass);
      const updateParent = await Parent.update({ password: newPassword },
        { where: { id: id } }
      );
      return sendResponse(res, "success", 202, "Password updated");
    }
  } catch (err) {
    console.log('error updating parent password', err);
    return sendResponse(res, "fail", 400, "Something went wrong updating password, try again");
  };
};

const UpdateChildPassword = async (req, res) => {
  const { id, current_pass, new_pass } = req.body;
  try {
    const child = await Child.findOne({
      where: { id: id },
      attributes: ["password"],
      raw: true
    });
    const passwordMatched = await isPasswordMatch(current_pass, child.password);
    if (!passwordMatched) {
      return sendResponse(res, "fail", 401, "Your current password is incorrect, re-enter and try again");
    } else {
      const newPassword = await hashPassword(new_pass);
      const updateChild = await Child.update({ password: newPassword },
        { where: { id: id } }
      );
      return sendResponse(res, "success", 202, "Password updated successfully");
    }
  } catch (err) {
    console.log('error updating child password', err);
    return sendResponse(res, "fail", 400, "Something went wrong updating password, try again");
  };
};

const UpdateEmail = async (req, res) => {
  const { id, email } = req.body;
  try {
    const checkForEmail = await Parent.findOne({
      where: { email: email },
      raw: true
    });
    if (checkForEmail) {
      return sendResponse(res, "fail", 409, "Account with this email already exists, please enter a new one");
    } else {
      const update = await Parent.update({ email: email },
        {
          returning: true,
          raw: true,
          where: { id: id }
        }
      );
      delete update[1][0].password;

      return sendResponse(res, "success", 202, "Email updated successfully", update[1][0]);
    }
  } catch (err) {
    console.log('error updating email', err);
    return sendResponse(res, "fail", 400, err.message);
  };
};

const UpdateChildUsername = async (req, res) => {
  const { id, username } = req.body;
  try {
    const checkForUsername = await Child.findOne({
      where: { username: username },
      raw: true,
    });
    if (checkForUsername) {
      return sendResponse(res, "fail", 409, "Account with this username already exists, please enter a new one");
    } else {
      const update = await Child.update({ username: username },
        {
          returning: true,
          raw: true,
          where: { id: id }
        }
      );
      delete update[1][0].password;
      return sendResponse(res, "success", 202, "Username updated successfully", update[1][0]);
    }
  } catch (err) {
    console.log('error updating child username', err);
    return sendResponse(res, "fail", 400, err.message);
  }
}

const UpdatePhoneNumber = async (req, res) => {
  const { id, phoneNumber } = req.body;
  try {
    const update = await Parent.update({ phoneNumber: phoneNumber },
      {
        returning: true,
        raw: true,
        where: { id: id },
      }
    );
    delete update[1][0].password;
    return sendResponse(res, "success", 202, "Phone number updated successfully", update[1][0]);
  } catch (err) {
    console.log('error updating phone number', err);
    return sendResponse(res, "fail", 400, err.message);
  };
};

const ForgotPassword = async (req, res) => {
  const { username, email } = req.body;
  const newPw = createRandomPassword();
  try {
    if (username) {
      const checkForChild = await Child.findOne({
        where: { username: username },
        attributes: ["id", "parentId", "username"],
        raw: true,
      });
      if (checkForChild) {
        const parentEmail = await Parent.findOne({
          where: { id: checkForChild.parentId },
          attributes: ["email"],
          raw: true,
        });
        const send = await ForgotPasswordEmail(parentEmail.email, `${checkForChild.username}`, newPw);
        const hashedPw = await hashPassword(newPw);
        const update = Child.update({ password: hashedPw },
          { where: { id: checkForChild.id } }
        );
        return sendResponse(res, "success", 202, "We just emailed your parent a temporary password for you");
      } else {
        return sendResponse(res, "fail", 401, "Account with this username doesn't exist");
      }
    } else {
      const checkForParent = await Parent.findOne({
        where: { email: email },
        attributes: ["id", "firstName", "lastName"],
        raw: true,
      });
      if (checkForParent) {
        const send = await ForgotPasswordEmail(email, `${checkForParent.firstName} ${checkForParent.lastName}`, newPw);
        const hashedPw = await hashPassword(newPw);
        const update = await Parent.update({ password: hashedPw },
          { where: { id: checkForParent.id } }
        );
        return sendResponse(res, "success", 202, "We just emailed you a temporary password");
      } else {
        return sendResponse(res, "fail", "Account with this email doesn't exist");
      };
    };
  } catch (err) {
    console.log('error creating new password', err);
    return sendResponse(res, "fail", 400, err.message);
  };
};

module.exports = {
  UpdateParentPassword,
  UpdateChildPassword,
  UpdateEmail,
  UpdateChildUsername,
  UpdatePhoneNumber,
  ForgotPassword
}; 