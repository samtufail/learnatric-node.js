const { Parent, Child } = require("../models");
const { Sequelize } = require("sequelize");
const { hashPassword, isPasswordMatch, sendResponse, JWT_SignIn } = require("../Utils");
const { Stripe } = require("../fixture");

const SignUp = async (req, res) => {
  let { firstName,
    lastName,
    email,
    phoneNumber,
    password,
    hearedFrom,
    childs,
    paymentMethod,
    plan_selected,
    is_free_signup
  } = req.body;

  const isParentAlreadyExist = await Parent.findOne({ where: { email: email } });
  if (isParentAlreadyExist) {
    return sendResponse(res, "fail", 409, "Email Already Exists");
  }

  let stripeId, stripeSubsID
  if (!is_free_signup) {
    const paymentMethodID = paymentMethod.id;

    const customer = await Stripe.createCustomer(paymentMethodID, email);
    const subscription = await Stripe.createSubscription(customer.id, plan_selected, childs);

    stripeId = customer.id;
    stripeSubsID = subscription.id;
  }
  password = await hashPassword(password);

  const parent = {
    firstName,
    lastName,
    email,
    phoneNumber,
    password,
    hearedFrom,
    childs,
    stripeId: is_free_signup ? null : stripeId,
    stripeSubsID: is_free_signup ? null : stripeSubsID,
    is_free_signup
  };
  const createdParent = await Parent.create(parent);

  return sendResponse(res, "success", 201, "Parent Created Successfully", createdParent);
};

const Login = async (req, res) => {
  const { username, password } = req.body;

  if (username.includes("@")) {
    const parent = await Parent.findOne({
      where: { email: username },
      raw: true,
    });
    // console.log('parent: ', parent)
    if (parent) {
      // const isSubValid = await Stripe.checkSubscription(parent.stripeSubsID)
      const passwordMatched = await isPasswordMatch(password, parent.password);
      if (passwordMatched) {
        const type = "PARENT";
        const { email, id, step } = parent;
        const token = await JWT_SignIn({ email, id, step, type });
        delete parent.password;
        return sendResponse(res, "success", 200, "Parent Logged In Successfully", { parent, token, type });
      } else {
        return sendResponse(res, "fail", 401, "Wrong User Credentials");
      }
    } else {
      return sendResponse(res, "fail", 401, "Wrong User Credentials");
    }
  } else {
    const child = await Child.findOne({
      where: { username: username },
      raw: true,
    });
    if (child) {
      if (!child.is_active_subscription) {
        return sendResponse(res, "fail", 401, "The subscription for this account has been canceled. Please log in to the parent dashboard to reactivate it.")
      }
      const passwordMatched = await isPasswordMatch(password, child.password);
      if (passwordMatched) {
        const type = "CHILD";
        const { username, id } = child;
        const token = await JWT_SignIn({ username, id, type });
        delete child.password;
        return sendResponse(res, "success", 200, "Child Logged In Successfully", { child, token, type });
      } else {
        return sendResponse(res, "fail", 401, "Wrong User Credentials");
      }
    } else {
      return sendResponse(res, "fail", 401, "Wrong User Credentials");
    }
  }
};

const FetchChildData = async (req, res) => {
  const { id } = req.query
  const children = await Child.findAll({
    where: { parentId: id },
    attributes: ["id", "firstName", "grade", "avatarSmall", "is_active_subscription"],
    raw: true
  });
  return sendResponse(res, "success", 200, "found children", children);
}

const ChildrenUpdate = async (req, res) => {
  const {
    childs,
    id,
    subId,
    childId,
    isLessChildren,
    noOfChildsCredsCompleted,
    noOfChildsProfileCompleted
  } = req.body;
  const update = await Stripe.updateQuantity(subId, childs);
  if (update) {
    if (isLessChildren) {
      const lessChilds = await Child.update(
        { is_active_subscription: false },
        { where: { id: childId } }
      )
      const removeChilds = await Parent.update(
        {
          childs: childs,
          noOfChildsProfileCompleted: noOfChildsProfileCompleted - 1,
          noOfChildsCredsCompleted: noOfChildsCredsCompleted - 1
        },
        { where: { id: id } }
      )
      const parent = await Parent.findOne({
        where: { id: id },
        raw: true,
      })
      delete parent.password;
      return sendResponse(res, 'success', 200, "Number of children updated", parent)
    } else {
      const newChilds = await Parent.update(
        {
          childs: childs,
          step: 1
        },
        {
          where: { id: id }
        }
      )
      const parent = await Parent.findOne({
        where: { id: id },
        raw: true,
      })
      delete parent.password;
      return sendResponse(res, 'success', 200, "Number of children updated", parent)
    }
  } else {
    return sendResponse(res, 'fail', 401, "Number of children could not be updated")
  }
}

const BillingInfo = async (req, res) => {
  const { subId, custId } = req.query;
  try {
    const check = await Stripe.checkSubscription(subId)
    if (check) {
      return sendResponse(res, 'success', 200, "Billing info found", check)
    } else {
      return sendResponse(res, 'fail', 401, "Customer info not found")
    }
  } catch (e) {
    return sendResponse(res, 'fail', 401, "Customer info not found")
  }
}

const CancelSub = async (req, res) => {
  const { id, stripeSubsID } = req.body;
  try {
    const cancelSub = await Stripe.cancelSubscription(stripeSubsID);
    const updatedParent = await Parent.update({ did_cancel_subscription: true }, {
      where: { id: id },
    })
    return sendResponse(res, "success", 200, "Subscription will be canceled at the end of the month, you will no longer be charged", cancelSub);
  } catch (e) {
    console.log('Error canceling subscription', e)
    return sendResponse(res, 'fail', 400, e.message)
  }

}

const ResumeSub = async (req, res) => {
  const { id, stripeSubsID } = req.body;
  try {
    const resumeSub = await Stripe.resumeSubscription(stripeSubsID);
    const updatedParent = await Parent.update({ did_cancel_subscription: false }, {
      where: { id: id },
    })
    return sendResponse(res, "success", 200, "Subscription was successfully resumed", resumeSub);
  } catch (e) {
    console.log('Error resuming subscription', e)
    return sendResponse(res, 'fail', 400, e.message)
  }
}

const UpdateCard = async (req, res) => {
  // console.log('body: ', req.body)
  const { custId, paymentMethod, values, subId } = req.body;
  const updateCard = await Stripe.updatePaymentMethod(subId, custId, paymentMethod, values.email)
  if (updateCard) {
    return sendResponse(res, 'success', 200, "Payment Info Updated", updateCard)
  } else {
    return sendResponse(res, 'fail', 401, "Error Updating Payment Method")
  }
}


module.exports = {
  SignUp,
  Login,
  CancelSub,
  BillingInfo,
  ChildrenUpdate,
  FetchChildData,
  UpdateCard,
  ResumeSub
};
