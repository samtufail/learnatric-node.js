const { Question, TagRating, Answer, Child, Lesson } = require("../models");
// const questionsJSON = require("../data/questions.json");
const { sendResponse } = require("../Utils");

const createQuestionMapWithAttempts = (questions, ansArr) => {
  // console.log('questions', ansArr)
  const qMap = new Map();

  questions.forEach((quest) => qMap.set(quest.question_id, quest));
  // console.log('questions', qMap)
  if (ansArr) {
    ansArr.forEach((ans) => {
      let question = qMap.get(ans);
      if (question.times_attempted) {
        let times = question.times_attempted + 1;
        question = {
          ...question,
          times_attempted: times,
        };
      } else {
        question = {
          ...question,
          times_attempted: 1,
        };
      }
      qMap.set(ans, question);
    });
  }
  return qMap;
};

const calcVarience = (rating, map) => {
  // console.log("ratinfg: ", rating);
  const qVArience = [];
  map.forEach((question) => {
    let varience;
    if (question.question_rating > rating) {
      varience = (question.question_rating - rating) * 0.3;
    } else {
      varience = (rating - question.question_rating) * 0.7;
    }
    varience = question.times_attempted
      ? varience + question.times_attempted * 20
      : varience;
    qVArience.push({
      ...question,
      varience,
    });
  });
  // console.log('var: ', qVArience)
  return qVArience.sort((a, b) => {
    return a.varience - b.varience;
  });
};

const curatedQuestion = async (req, res) => {
  const { child_id, tag_id, lesson_id } = req.query;
  // console.log("query: ", req.query);

  // const questions = await Question.findAll({
  //   where: { p_tag: tag_id },
  //   order: [["question_rating", "ASC"]],
  //   raw: true,
  // });
  //find all questions for this lesson
  const lessonWithQuestions = await Lesson.findOne({
    where: { id: lesson_id },
    include: {
      model: Question,
    },
  });
  // console.log("questions: ", lessonWithQuestions.Questions);
  // create array of just the question ids to use later to see if the student ever answered this question before
  const questionIds = [];
  const justQuestions = lessonWithQuestions.Questions.map((question) => {
    questionIds.push(question.dataValues.question_id);
    delete question.dataValues.L_Q_Relationship;
    return question.dataValues;
  });
  // console.log("q ids: ", questionIds);
  // get the students tag rating for this tag
  const rating = await TagRating.findOne({
    where: {
      child_id,
      tag_id,
    },
    attributes: ["tag_rating"],
    raw: true,
  });
  // use the array of question ids to search the answer_results table for any records with this child_id
  const answers = await Answer.findAll({
    where: {
      child_id,
      question_id: questionIds,
    },
    raw: true,
  });
  // console.log('rating a qs: ', answers)
  const answersArray =
    answers.length > 0 ? answers.map((answer) => answer.question_id) : null;
  const questionMap = createQuestionMapWithAttempts(
    justQuestions,
    answersArray
  );
  const questionVarience = calcVarience(rating.tag_rating, questionMap);
  // console.log("question return: ", questionVarience[0]);
  const updateChild = await Child.update(
    {
      question_id: questionVarience[0].question_id,
    },
    {
      where: { id: child_id },
      returning: true,
      raw: true,
    }
  );
  // console.log("child: ", updateChild);
  const data = {
    question: questionVarience[0],
    updateChild,
  };
  // console.log("q id curATED: ", questionVarience[0].question_id);

  return sendResponse(res, "success", 200, "Curated ideal question", data);
};

const fetchQuestionById = async (req, res) => {
  const { question_id } = req.query;
  const question = await Question.findOne({
    where: { question_id: question_id },
    raw: true,
  });
  // console.log("question: ", question);
  return sendResponse(res, "success", 200, "Fetched Question by id", question);
};
module.exports = {
  curatedQuestion,
  fetchQuestionById,
};
