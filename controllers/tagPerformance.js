const { Child, Parent, Tag, TagRating, TagPerformance } = require("../models");

const { sendResponse } = require("../Utils");

const updateTP = async (req, res) => {
  const {
    start_at,
    child_id,
    grade,
    createdAt,
    updatedAt,
  } = req.body;

  const tags = await Tag.findAll();
  const startRating = grade === 'Pre-K' ? 30 : grade === 'Kindergarten' ? 50 : grade === 'First' ? 100 : grade === 'Second' ? 200 : 300;

  const tagPerformanceStart = tags.map((item, idx) => {
    return {
      performance_id: start_at + idx,
      child_id: child_id,
      question_id: 0,
      lesson_id: 0,
      tag_id: item.dataValues.tag_id,
      question_rating: startRating,
      tag_rating_start: startRating,
      is_correct: true,
      rating_new: startRating,
      is_we_do: false,
      createdAt: createdAt,
      updatedAt: updatedAt,
    }
  })
  // console.log('tags perf: ', tagPerformanceStart)
  try {
    await TagPerformance.bulkCreate(tagPerformanceStart)
  } catch (e) {
    console.log('error tagPer bulk insert: ', e.message)
  }
}

module.exports = { updateTP };
