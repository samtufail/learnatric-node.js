const { Lesson, Child, Question } = require('../models');
const { LessonCuration } = require('../services/lessonCurationService');
// const lessonsJSON = require("../data/lessons.json");

const { sendResponse, getGradeID } = require('../Utils');

const getLesson = async (findBy) => {
  const lessonAndQuestions = await Lesson.findOne({
    where: typeof findBy === 'string' ? { grade: findBy } : { id: findBy },
    include: {
      model: Question,
    },
  });
  // console.log('lesson', lesson.dataValues)
  // const { Questions } = lessonAndQuestions.dataValues;
  // eslint-disable-next-line no-shadow
  const lesson = (({ Questions, ...lesson }) => lesson)(
    lessonAndQuestions.dataValues
  );
  // // console.log('clone: ', lesson)
  // let questions = Questions.map((question) => {
  //   delete question.dataValues.L_Q_Relationship;
  //   return question.dataValues;
  // });
  // questions = questions.sort(() => 0.5 - Math.random()).slice(0, 5);
  // return { lesson, questions }
  return { lesson };
};

const getLessons = async (req, res) => {
  const { childId } = req.query;

  // Get the Grade
  const child = await Child.findOne({ where: { id: childId }, raw: true });
  const grade = getGradeID(child.grade);

  if (child.isFirstLogin) {
    await Child.update(
      { isFirstLogin: false },
      {
        where: { id: childId },
        returning: true,
        raw: true,
      }
    );
    // return sendResponse(res, "success", 200, "Get Lessons", firstLesson);
  }
  // lesson curation
  const curate = new LessonCuration();
  if (child.lesson_id) {
    // console.log('has id: ', child.lesson_id)
    const lesson = await getLesson(child.lesson_id);
    // console.log('chld lesson: ', lesson)
    return sendResponse(res, 'success', 200, 'Get Lessons', lesson);
  }
  const nextLesson = await curate.curateIdealLesson({
    child_id: childId,
    grade,
  });
  // just curate the lesson here not the Questions
  const lesson = await getLesson(nextLesson);
  // console.log('chld lesson: ', lesson)
  await Child.update(
    { lesson_id: lesson.lesson.id },
    {
      where: { id: childId },
      returning: true,
      plain: true,
      raw: true,
    }
  );
  // console.log('child update after finishing lesson: ', childUpdate)
  return sendResponse(res, 'success', 200, 'Get Lessons', lesson);
};
module.exports = {
  getLessons,
};
