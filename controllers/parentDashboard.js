const { Child, Parent } = require('../models');
const {
  getTopAndBottom3,
  fetchDomainAvgLast5,
  averageTheAverages,
  getDomainsByGarde,
  formatAllAverages,
  // timeSpentAndLessonCount
} = require('../Utils/parentDashUtils');
const { getGradeID, sendResponse, JWT_SignIn } = require('../Utils/index');

const fetchChildDomainData = async (req, res) => {
  // console.log('query: ', req.query)
  const { parent_id } = req.query;
  const children = await Child.findAll({
    where: { parentId: parent_id },
    attributes: [
      'id',
      'firstName',
      'grade',
      'avatarSmall',
      'is_active_subscription',
      'assessment_complete',
    ],
    raw: true,
  });
  console.log('kids: ', children);
  let result = [];
  if (children.length > 1) {
    // create promise array
    children.forEach(async (kid) => {
      const grade = getGradeID(kid.grade);
      const topAndBottomRatings = await getTopAndBottom3(kid.id, grade);
      const domainesByGrade = await getDomainsByGarde(grade);
      const averagesByDomaine = await fetchDomainAvgLast5(kid.id);
      const averageAll = averageTheAverages(averagesByDomaine, domainesByGrade);
      const format = formatAllAverages(averageAll);
      // const getTimeSpent = await timeSpentAndLessonCount(kid.id);
      result = [
        ...result,
        {
          assessment_complete: kid.assessment_complete,
          firstName: kid.firstName,
          childId: kid.id,
          avatar: kid.avatarSmall,
          is_active_subscription: kid.is_active_subscription,
          topAndBottom: topAndBottomRatings,
          formattedAvgs: format,
          // timeAndLessonCount: getTimeSpent
        },
      ];
    });
  } else {
    const grade = getGradeID(children[0].grade);
    const topAndBottomRatings = await getTopAndBottom3(children[0].id, grade);
    const domainesByGrade = await getDomainsByGarde(grade);
    const averagesByDomaine = await fetchDomainAvgLast5(children[0].id);
    const averageAll = averageTheAverages(averagesByDomaine, domainesByGrade);
    // console.log('averageAll: ', averageAll);
    const format = formatAllAverages(averageAll);
    // const getTimeSpent = await timeSpentAndLessonCount(children[0].id);
    result = [
      ...result,
      {
        assessment_complete: children[0].assessment_complete,
        firstName: children[0].firstName,
        childId: children[0].id,
        avatar: children[0].avatarSmall,
        topAndBottom: topAndBottomRatings,
        formattedAvgs: format,
        // timeAndLessonCount: getTimeSpent
      },
    ];
    // console.log('singleKid: ', result)
  }
  sendResponse(res, 'success', 200, 'Fetched and Averaged Child Data', result);
};

const switchToKid = async (req, res) => {
  const { childId } = req.query;
  const child = await Child.findOne({
    where: { id: childId },
    raw: true,
  });
  const type = 'CHILD';
  const { username, id } = child;
  const token = await JWT_SignIn({ username, id, type });
  delete child.password;
  return sendResponse(res, 'success', 200, 'Switched to Child Successfully', {
    child,
    token,
    type,
  });
};

const switchToParent = async (req, res) => {
  const { childId } = req.query;
  const parentId = await Child.findOne({
    where: { id: childId },
    raw: true,
    attributes: ['parentId'],
  });
  if (parentId) {
    const parent = await Parent.findOne({
      where: { id: parentId.parentId },
      raw: true,
    });
    const type = 'PARENT';
    const { email, id, step } = parent;
    const token = await JWT_SignIn({ email, id, step, type });
    delete parent.password;
    return sendResponse(res, 'success', 200, 'Parent Logged In Successfully', {
      parent,
      token,
      type,
    });
  }
  return sendResponse(res, 'fail', 401, 'Error Finding Parent');
};

module.exports = {
  fetchChildDomainData,
  switchToKid,
  switchToParent,
};
