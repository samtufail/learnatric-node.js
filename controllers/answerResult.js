const { Answer, Child, TagPerformance, TagRating, Question, Lesson } = require("../models");
const { sendResponse, getQuestionTags, tagCalculationFormula } = require("../Utils");

//this function dynamically adjusts Question ratings
const calcQsAndUpdateDB = async (data, tag = null) => {
  const { answerResult, question, } = data;
  const { child_id, is_correct, is_we_do } = answerResult;
  const tagID = tag ? tag : question.tag_id

  const oldTagRating = await TagRating.findOne({
    where: { child_id: child_id, tag_id: tagID },
    returning: true,
    raw: true
  })
  const Qtarget = is_we_do ? .7 : question.target
  const newQuestionRating = await tagCalculationFormula(Number.parseInt(question.question_rating), oldTagRating.tag_rating, is_correct, Qtarget, false)
  if (!is_we_do) {
    const updateQuestion = await Question.update({ question_rating: newQuestionRating },
      {
        where: { question_id: question.question_id },
        returning: true,
      }
    )
  } else {
    const updateLesson = await Lesson.update({ question_rating: newQuestionRating },
      { where: { id: question.id }, returning: true, plain: true },
    )
  }
}

//this function calculates and updates tag ratings then populates a record in the Tag_Performance table
const calcTagsAndUpdateDB = async (data, tag = null) => {
  const { answerResult, question, } = data;
  const { child_id, lesson_id, question_id, is_correct, is_we_do } = answerResult;
  const tagID = tag ? tag : question.tag_id

  const oldTagRating = await TagRating.findOne({
    where: { child_id: child_id, tag_id: tagID },
    returning: true,
    raw: true
  })
  const Qtarget = is_we_do ? .7 : question.target
  const newTagRating = await tagCalculationFormula(oldTagRating.tag_rating, Number.parseInt(question.question_rating), is_correct, Qtarget, true);
  // const newQuestionRating = await tagCalculationFormula(Number.parseInt(question.question_rating), oldTagRating.tag_rating, is_correct, Qtarget, false)

  const tagRatingdata = { tag_rating: newTagRating }
  const tagPerfData = {
    child_id,
    question_id,
    lesson_id,
    tag_id: tagID,
    question_rating: Number.parseInt(question.question_rating),
    tag_rating_start: oldTagRating.tag_rating,
    is_correct,
    rating_new: newTagRating,
    is_we_do
  }
  const ratingUpdate = await TagRating.update(tagRatingdata, {
    where: { child_id: child_id, tag_id: tagID }
  })
  // console.log('rating updated: ', ratingUpdate)
  const perfUpdate = await TagPerformance.create(tagPerfData)

  // console.log('perf updated: ', perfUpdate)
}

const createPromiseArray = (tags, data) => {
  const promiseArray = [];
  tags.forEach(tag => {
    promiseArray.push(calcTagsAndUpdateDB(data, Number.parseInt(tag)))
  });
  return promiseArray;
}

const addAnswerResult = async (req, res, next) => {
  const { answerResult, question, lastQuestion, questionIndex } = req.body;
  const { child_id, lesson_id, question_id, text_answer, is_correct, is_we_do } = answerResult;
  const newAnswer = {
    // parent_id,
    child_id,
    lesson_id,
    question_id,
    text_answer,
    is_correct,
    is_we_do
  };
  const newAnswerResult = await Answer.create(newAnswer);

  let updatedChild
  if (is_we_do) {
    await calcTagsAndUpdateDB(req.body)
    const qUpdate = await calcQsAndUpdateDB(req.body);
  } else {
    if (lastQuestion) {
      updatedChild = await Child.update({ stepInLesson: null, questionIndex: null, lesson_id: null, user_lesson_id: null },
        {
          where: { id: child_id },
          returning: true,
          plain: true,
          raw: true
        }
      );
    } else {
      updatedChild = await Child.update({ questionIndex: questionIndex },
        {
          where: { id: child_id },
          returning: true,
          plain: true,
          raw: true
        });
    }
    const tagIDs = getQuestionTags(question);
    const qUpdate = await calcQsAndUpdateDB(req.body, tagIDs[0]);
    if (tagIDs.length > 1) {
      const promises = createPromiseArray(tagIDs, req.body);
      const solved = await Promise.allSettled(promises);
    } else {
      await calcTagsAndUpdateDB(req.body, tagIDs[0]);
    }
  }

  return sendResponse(res, "success", 200, "AnswerResult Added Successfully", updatedChild);
};

module.exports = {
  addAnswerResult,
};
