const { UserLesson, Child, Answer, Lesson, TagPerformance, Tag } = require("../models");
const { sendResponse, getGradeID } = require("../Utils");
var Sequelize = require('sequelize');
const moment = require('moment');
const c = require("config");
var Op = Sequelize.Op;

module.exports = {
  UserMetrics: async (req, res) => {
    const { hours } = req.query;
    const allChildren = await Child.findAll({ 
      where: { is_test_account: false },
      attributes: ["id", "firstName", "grade", "username"],
      raw: true,
    })
    // console.log('children: ', allChildren)
    const childIds = allChildren.map(child => child.id);
    
    const usersLessons = await UserLesson.findAll({
      where: {
        child_id: { 
          [Op.in]: childIds
        }
      },
      raw: true,
    });
    const currentTime = moment().unix();
    const subtractTime = moment().subtract(hours, 'hours').unix();
    const filterLessons = usersLessons.filter(lesson => {
      let createdAtUnix = moment(lesson.createdAt).unix();
      if (createdAtUnix <= currentTime && createdAtUnix >= subtractTime) {
        return true
      } else {
        return false
      }
    })
    const newChildIds = filterLessons.map(lesson => lesson.child_id);
    // console.log('child ids: ', newChildIds)
    const lessonIds = filterLessons.map(lesson => lesson.lesson_id);
    // console.log('lessonIds: ', lessonIds)
    const answers = await Answer.findAll({
      where: {
        child_id: {
          [Op.in]: newChildIds
        },
        lesson_id: {
          [Op.in]: lessonIds
        }
      },
      raw: true,
    })
    const filterAnswers = answers.filter(answer => {
      let createdAtUnix = moment(answer.createdAt).unix();
      if (createdAtUnix <= currentTime && createdAtUnix >= subtractTime) {
        return true
      } else {
        return false
      }
    })
    // console.log('answer ids: ', filterAnswers)
    const lessonTags = await Lesson.findAll({
      where: {
        id: { 
          [Op.in]: lessonIds
        }
      },
      include: "Tag",
      attributes: [["id", "lessonId"], "lesson_name"],
      raw: true
    })
    // console.log('lesson tags: ', lessonTags)
    let childLessonAnswer = {};
    const childMap = new Map();
    const lessonMap = new Map();
    allChildren.forEach(child => childMap.set(child.id, child));
    lessonTags.forEach(lesson => lessonMap.set(lesson.lessonId, lesson));
    // console.log('child map: ', lessonMap)
    for (let answer of filterAnswers) {
      let child = childMap.get(answer.child_id);
      let lesson = lessonMap.get(answer.lesson_id);
      if (!childLessonAnswer[child.firstName]) {
        childLessonAnswer[child.firstName] = { 
          child_info: child, 
          lessons: {
            [lesson.lessonId]: {
              lesson_tag_info: lesson,
              answers: [answer]
            }
          } 
        };
      } else {
        if (!childLessonAnswer[child.firstName].lessons[lesson.lessonId]) {
          childLessonAnswer[child.firstName].lessons[lesson.lessonId] = { lesson_tag_info: lesson, answers: [answer] }
        } else {
          childLessonAnswer[child.firstName].lessons[lesson.lessonId].answers = [
            ...childLessonAnswer[child.firstName].lessons[lesson.lessonId].answers,
            answer
          ]
        }
      }
    }
    childLessonAnswer = {
      all_children: allChildren,
      ...childLessonAnswer,
    }
    return sendResponse(res, "success", 200, "got all data", childLessonAnswer)
  },

  AverageTagChange: async (req, res) => {
    const { start, end } = req.query;
    const startUnix = moment().subtract(`${start}`, 'days').unix();
    const endUnix = moment().subtract(`${end}`, 'days').unix(); 

    // const allTagPerf = await TagPerformance.findAll({ 
    //   attributes: ["performance_id", "child_id", "tag_id", "tag_rating_start", "rating_new", "createdAt"],
    //   include: { model: Child},
    //   raw: true 
    // });
    const allTagPerf = await Child.findAll({
      where: { is_test_account: false },
      attributes: ["id", "firstName", "grade"],
      include: { 
        model: TagPerformance, 
        as: "child_Tag_Performance", 
        attributes: ["performance_id", "child_id", "tag_id", "tag_rating_start", "rating_new", "createdAt"],
        include: {
          model: Tag,
          attributes: ["tag_id", "grade"]
        }
      },
      raw: true
    })
    const filterNonGrade = allTagPerf.filter((childTagPerf) => {
      let grade = getGradeID(childTagPerf.grade)
      return grade === childTagPerf['child_Tag_Performance.Tag.grade']
    })
    // console.log('filteredL ', filterNonGrade)
    const filteredTagPerf = filterNonGrade.filter(tagPerf => (
      moment(tagPerf["child_Tag_Performance.createdAt"]).unix() >= startUnix && moment(tagPerf["child_Tag_Performance.createdAt"]).unix() <= endUnix
      ))
      
    // console.log('all tags: ', filteredTagPerf)
    // // console.log('filtered length:', filteredTagPerf)
    const usersTagPerf = {}
    for (let perf of filteredTagPerf) {
      if (!usersTagPerf[perf.id]) {
        usersTagPerf[perf.id] = [perf]
      } else {
        usersTagPerf[perf.id].push(perf)
      }
    }
    
    // console.log('array: ', usersTagPerf)
    let finalUserTagChange = {}
    for (let user in usersTagPerf) {
      // console.log('user: ', usersTagPerf[user])
      // let tagMap = new Map();
      let tagMap = {}
      for (let tagPerf of usersTagPerf[user]) {
        if (!tagMap[tagPerf['child_Tag_Performance.tag_id']]) {
          tagMap[tagPerf['child_Tag_Performance.tag_id']] = {
            start: tagPerf['child_Tag_Performance.tag_rating_start'],
            end: tagPerf['child_Tag_Performance.rating_new']
          }
        } else {
          let startVal = tagMap[tagPerf['child_Tag_Performance.tag_id']].start
          tagMap[tagPerf['child_Tag_Performance.tag_id']] = {
            start: startVal,
            end: tagPerf['child_Tag_Performance.rating_new']
          }
        }
      }
      const numGradeTags = await Tag.findAll({
        where: { grade: usersTagPerf[user][0]['child_Tag_Performance.Tag.grade'] },
        raw: true
      })
      
      usersTagPerf[user] = {
        firstName: usersTagPerf[user][0].firstName,
        child_id: user,
        tagMap : tagMap,
        numOfTags: numGradeTags.length
      }
    }
    
    for (let user in usersTagPerf) {
      // console.log('user: ', usersTagPerf[user])
      let count = 0;
      let diff = 0
      for (let tag in usersTagPerf[user].tagMap) {
        // console.log('tag: ', tag)
        if (usersTagPerf[user].tagMap[tag].start !== usersTagPerf[user].tagMap[tag].end) {
          count++;
          let sub = usersTagPerf[user].tagMap[tag].end - usersTagPerf[user].tagMap[tag].start
          diff += sub
        }
      }
      usersTagPerf[user].map = {average: diff / usersTagPerf[user].numOfTags}
    }
    // console.log('new????: ', usersTagPerf)
    let arr = Object.values(usersTagPerf)

    let totalChange = 0

    arr.forEach(change => {
      totalChange += change.map.average
    })

    const totalChildCount = await Child.findAll({
      where : { is_test_account: false },
      raw: true
    })

    let data = {
      totalChange : totalChange,
      totalNumUsers: totalChildCount.length,
      averageChange: totalChange / totalChildCount.length,
      children : arr
    }
    return sendResponse(res, "success", 200, "got average tag change for time period", data)
  }

}