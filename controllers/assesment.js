const {
  Answer,
  UserLesson,
  Child,
  TagPerformance,
  TagRating,
  Question,
  Tag,
  Assessment,
} = require("../models");
const {
  sendResponse,
  tagCalculationFormula,
  getGradeID,
} = require("../Utils");
const moment = require("moment");

// const getFinalAssessmentIndex = (grade) => {
//   return 7;
// };

const regressToMean = async (child_id, grade) => {
  // fetch all the tag_ratings for this student in their grade level
  const tagRaings = await TagRating.findAll({
    where: {
      child_id: child_id,
      grade: grade,
    },
    include: {
      model: Tag,
      as: "Tag",
      attributes: ["domain_code"],
      raw: true,
    },
    raw: true,
  });

  //average tag_ratings by domain but dont include the tags that have not been touched
  const domainAverages = {};

  tagRaings.forEach((tag) => {
    //convert the createdAt and updatedAt timestamps into unix time
    let unixCreated = moment(tag.createdAt).unix();
    let unixUpdated = moment(tag.updatedAt).unix();
    // if domainAverages doesnt already have a property for the domain that the current tag is in
    if (!domainAverages[tag["Tag.domain_code"]]) {
      // if this that has been updated
      if (unixCreated !== unixUpdated) {
        domainAverages[tag["Tag.domain_code"]] = {
          count: 1,
          sum: tag.tag_rating,
          tags: [tag],
        };
      } else {
        domainAverages[tag["Tag.domain_code"]] = {
          count: 0,
          sum: 0,
          tags: [tag],
        };
      }
    } else {
      if (unixCreated !== unixUpdated) {
        domainAverages[tag["Tag.domain_code"]] = {
          ...domainAverages[tag["Tag.domain_code"]],
          count: domainAverages[tag["Tag.domain_code"]].count + 1,
          sum: domainAverages[tag["Tag.domain_code"]].sum + tag.tag_rating,
          tags: [...domainAverages[tag["Tag.domain_code"]].tags, tag],
        };
      } else {
        domainAverages[tag["Tag.domain_code"]] = {
          ...domainAverages[tag["Tag.domain_code"]],
          tags: [...domainAverages[tag["Tag.domain_code"]].tags, tag],
        };
      }
    }
  });

  // regress either up or down depending if the current score is higher or lower than the average by 30%?
  for (let key in domainAverages) {
    domainAverages[key]["domain_average"] = Number.parseInt(
      (domainAverages[key].sum / domainAverages[key].count).toFixed(2)
    );
    const { domain_average, tags } = domainAverages[key];
    for (let i = 0; i < tags.length; i++) {
      let tag = tags[i];
      let newRating = Number.parseInt(
        (tag.tag_rating * 0.7 + domain_average * 0.3).toFixed(2)
      );
      let update = await TagRating.update(
        {
          tag_rating: newRating,
        },
        {
          where: {
            tag_rating_id: tag.tag_rating_id,
          },
          returning: true,
          plain: true,
        }
      );
    }
  }
};

const initiateTagPerf = async (child_id) => {
  //fetch all tag_ratings for this child_id
  const tagRatings = await TagRating.findAll({
    where: { child_id: child_id },
    raw: true,
  });

  //map over tag_ratings and return new objects to fit tag_performance table
  const tagPerfs = tagRatings.map((tag) => {
    return {
      child_id: child_id,
      question_id: 0,
      lesson_id: 0,
      tag_id: tag.tag_id,
      question_rating: tag.tag_rating,
      tag_rating_start: tag.tag_rating,
      is_correct: true,
      rating_new: tag.tag_rating,
      is_we_do: false,
    };
  });
  //bulkCreate tag perf
  await TagPerformance.bulkCreate(tagPerfs);
};

const getAssessmentQuestions = async (req, res) => {
  const { child_id, grade } = req.query;
  const gradeNum = getGradeID(grade);

  //all assessment question_ids for this students grade gradeLevel
  const assQs = await Assessment.findAll({
    where: {
      grade: gradeNum,
    },
    raw: true,
  });
  console.log("assQs: ", assQs);
  //see if student had already answered any of the questions
  const answers = await Answer.findAll({
    where: { child_id: child_id },
    raw: true,
  });

  //fetch all this students tag ratings for their grade level
  const fetchRatings = await TagRating.findAll({
    where: { child_id: child_id, grade: gradeNum },
    include: {
      model: Tag,
      as: "Tag",
      attributes: ["domain_code"],
      raw: true,
    },
    raw: true,
  });

  //if sudent has not already started the assessment
  if (answers.length === 0) {
    const { tag_rating } = fetchRatings[0];

    let diff = null;
    let id, rating, domain_code;
    for (let i = 0; i <= 5; i++) {
      let currDiff;
      if (tag_rating > assQs[i].question_rating) {
        currDiff = tag_rating - assQs[i].question_rating;
      } else {
        currDiff = assQs[i].question_rating - tag_rating;
      }

      if (diff === null) {
        diff = currDiff;
        id = assQs[i].question_id;
        rating = assQs[i].question_rating;
      } else if (currDiff < diff) {
        diff = currDiff;
        id = assQs[i].question_id;
        rating = assQs[i].question_rating;
      }
      domain_code = assQs[i].domain_code;
    }

    const question = await Question.findByPk(id, { raw: true });
    console.log("question?  ", question);
    question.question_rating = rating;
    question["domain_code"] = domain_code;
    return sendResponse(
      res,
      "success",
      200,
      "fetched first round of assessment question",
      question
    );
  } else {
    // already started assessment
    const alreadyCompletedQuestions = [];
    const alreadyAskedTags = [];
    answers.forEach((answer) => {
      alreadyCompletedQuestions.push(answer.question_id);
      alreadyAskedTags.push(answer.tag_id);
    });
    //create hash map to average out the tagscores for each domain
    const domainAverageMap = {};
    //create tag_ratings hash map
    const tagRatingMap = {};
    fetchRatings.forEach((rating) => {
      tagRatingMap[rating.tag_id] = {
        rating: rating.tag_rating,
        domain_code: rating["Tag.domain_code"],
      };
      if (!domainAverageMap[rating["Tag.domain_code"]]) {
        domainAverageMap[rating["Tag.domain_code"]] = {
          count: 1,
          sum: rating.tag_rating,
        };
      } else {
        let count = domainAverageMap[rating["Tag.domain_code"]].count;
        let sum = domainAverageMap[rating["Tag.domain_code"]].sum;
        domainAverageMap[rating["Tag.domain_code"]] = {
          count: count + 1,
          sum: sum + rating.tag_rating,
        };
      }
    });
    //average domain scores
    for (let key in domainAverageMap) {
      domainAverageMap[key].domain_average = Number.parseInt(
        (domainAverageMap[key].sum / domainAverageMap[key].count).toFixed(2)
      );
    }
    const nextQs = [];
    for (let i = 0; i < assQs.length; i++) {
      if (alreadyAskedTags.indexOf(assQs[i].tag_id) > -1) {
        continue;
      } else if (nextQs.length > 0 && assQs[i].tag_id !== nextQs[0].tag_id) {
        break;
      } else {
        nextQs.push(assQs[i]);
      }
    }

    if (nextQs.length > 0) {
      let id, rating, domain_code;
      let tag_domain_average =
        domainAverageMap[nextQs[0].domain_code].domain_average;
      let diff = null;
      for (let i = 0; i < nextQs.length; i++) {
        let currDiff;
        if (tag_domain_average > nextQs[i].question_rating) {
          currDiff = tag_domain_average - nextQs[i].question_rating;
        } else {
          currDiff = nextQs[i].question_rating - tag_domain_average;
        }

        if (diff === null) {
          diff = currDiff;
          id = nextQs[i].question_id;
          rating = nextQs[i].question_rating;
        } else if (currDiff < diff) {
          diff = currDiff;
          id = nextQs[i].question_id;
          rating = nextQs[i].question_rating;
        }
        domain_code = nextQs[i].domain_code;
      }
      const question = await Question.findByPk(id, { raw: true });
      console.log("question already started  ", question);
      question.question_rating = rating;
      question["domain_code"] = domain_code;
      return sendResponse(
        res,
        "success",
        200,
        "fetched next round of assessment question",
        question
      );
    } else {
      //user has already been asked one question on each tag
      let id, rating, domain_code;
      for (let i = 0; i < assQs.length; i++) {
        if (alreadyCompletedQuestions.indexOf(assQs[i].question_id) === -1) {
          id = assQs[i].question_id;
          rating = assQs[i].question_rating;
          domain_code = assQs[i].domain_code;
          break;
        } else {
          continue;
        }
      }
      const question = await Question.findByPk(id, { raw: true });
      console.log("question already started  ", question);
      question.question_rating = rating;
      question["domain_code"] = domain_code;
      return sendResponse(
        res,
        "success",
        200,
        "fetched next round of assessment question",
        question
      );
    }
  }
};

const updateTagsAndAnswers = async (req, res) => {
  const {
    child_id,
    tag_ids,
    domain_code,
    is_correct,
    question_id,
    question_rating,
    text_answer,
  } = req.body;
  const child = await Child.findOne({
    where: { id: child_id },
    raw: true,
  });
  const childsGrade = getGradeID(child.grade);
  //get array of all tag_ids to be updated and find current rating for those tags
  let tagIds = [];
  let answerMap = {};
  tag_ids.forEach((id) => {
    if (id !== null) {
      tagIds.push(id);
      answerMap[id] = {
        child_id: child_id,
        domain_code: domain_code,
        is_correct: is_correct,
        question_id: question_id,
        question_rating: question_rating,
        tag_id: id,
      };
    }
  });

  const currRatings = await TagRating.findAll({
    where: { child_id, tag_id: tagIds },
    attributes: ["tag_id", "tag_rating"],
    raw: true,
  });

  currRatings.forEach(
    (rating) =>
    (answerMap[rating.tag_id] = {
      ...answerMap[rating.tag_id],
      curr_rating: rating.tag_rating,
    })
  );
  //update tag_ratings table
  for (let key in answerMap) {
    const ratingUpdate = await TagRating.update(
      {
        tag_rating: tagCalculationFormula(
          answerMap[key].curr_rating, //current rating
          answerMap[key].question_rating, // qwuestion rating
          answerMap[key].is_correct, // if student answered correct or not
          0.1, //target rating
          true, //is_tag
          true //is_assessment
        ),
      },
      { where: { child_id, tag_id: Number.parseInt(key) } }
    );
  }
  //add record to answer table
  const newAnswer = await Answer.create({
    child_id: child_id,
    question_id: question_id,
    text_answer: text_answer,
    is_correct: is_correct,
    is_we_do: false,
    tag_id: Number.parseInt(tag_ids),
    domain_code: domain_code,
  });
  //update assessment_index on child table
  const updateChild = await Child.increment("assessment_index", {
    where: { id: child_id },
  });
  // if the assessment_index === 30
  if (updateChild[0][0][0].assessment_index === 30) {
    const updateComplete = await Child.update(
      { assessment_complete: true },
      { where: { id: child_id }, returning: true, plain: true, raw: true }
    );
    updateChild[0][0][0] = updateComplete[1];
    //regress to mean for all tag scores
    regressToMean(child_id, childsGrade);
    // initiate tag_performance table with all new tag scores
    initiateTagPerf(child_id);
  }
  delete updateChild[0][0][0].password;
  return sendResponse(
    res,
    "success",
    200,
    "Updated tag ratings, answer_results and child tables",
    updateChild[0][0][0]
  );
};

// just used for setting up reference tables in the begining
const getTagsForDomain = async (req, res) => {
  const { domain_code, grade } = req.query;
  const gradeNum = Number.parseInt(grade);
  console.log("query: ", req.query);
  const domaineTags = await Tag.findAll({
    where: { domain_code, grade },
    attributes: ["tag_id"],
    raw: true,
  });
  console.log("tags: ", domaineTags);
};
// just used for setting up reference tables in the begining
const getQsForTag = async (req, res) => {
  const { tag } = req.query;
  const questions = await Question.findAll({
    where: { p_tag: tag },
    attributes: ["question_id", "question_rating", "p_tag"],
    raw: true,
  });
  console.log("questions: ", questions);
};

module.exports = {
  getTagsForDomain,
  getQsForTag,
  getAssessmentQuestions,
  updateTagsAndAnswers,
};

/*
const getAssessmentQuestions = async (req, res) => {
  const { child_id, grade } = req.query;
  const gradeNum = getGradeID(grade);
  console.log("query: ", req.query, " gradeL ", gradeNum);

  //arrays of promises to fetch question ids for each grade level in each domain
  const gradeDomains = {
    0: [
      CountingCard.findAll({ where: { grade: gradeNum }, raw: true }),
      OperationAlgebraic.findAll({ where: { grade: gradeNum }, raw: true }),
      // NumbersBase.findAll({ where: { grade: gradeNum }, raw: true }),
      MeasureData.findAll({ where: { grade: gradeNum }, raw: true }),
      Geometry.findAll({ where: { grade: gradeNum }, raw: true }),
    ],
    1: [
      OperationAlgebraic.findAll({ where: { grade: gradeNum }, raw: true }),
      NumbersBase.findAll({ where: { grade: gradeNum }, raw: true }),
      MeasureData.findAll({ where: { grade: gradeNum }, raw: true }),
      Geometry.findAll({ where: { grade: gradeNum }, raw: true }),
    ],
    2: [
      OperationAlgebraic.findAll({ where: { grade: gradeNum }, raw: true }),
      NumbersBase.findAll({ where: { grade: gradeNum }, raw: true }),
      MeasureData.findAll({ where: { grade: gradeNum }, raw: true }),
      Geometry.findAll({ where: { grade: gradeNum }, raw: true }),
    ],
    3: [
      OperationAlgebraic.findAll({ where: { grade: gradeNum }, raw: true }),
      NumbersBase.findAll({ where: { grade: gradeNum }, raw: true }),
      MeasureData.findAll({ where: { grade: gradeNum }, raw: true }),
      NumbersFractions.findAll({ where: { grade: gradeNum }, raw: true }),
    ],
  };
  //see if student had already answered any of the questions
  const answers = await Answer.findAll({
    where: { child_id: child_id },
    raw: true,
  });

  //fetch all this students tag ratings for their grade level
  const fetchRatings = await TagRating.findAll({
    where: { child_id: child_id, grade: gradeNum },
    include: {
      model: Tag,
      as: "Tag",
      attributes: ["domain_code"],
      raw: true,
    },
    raw: true,
  });

  //find the question ids in each domain where the question rating is closest to the students rating
  const allIds = await Promise.all(gradeDomains[gradeNum]);
  // if you havent already started the assessment
  if (answers.length === 0) {
    const { tag_rating } = fetchRatings[0];
    const questionIds = allIds.map((domain) => {
      let diff = null;
      let id, rating;
      for (let i = 0; i < 5; i++) {
        let currDiff;
        if (tag_rating > domain[i].question_rating) {
          currDiff = tag_rating - domain[i].question_rating;
        } else {
          currDiff = domain[i].question_rating - tag_rating;
        }

        if (diff === null) {
          diff = currDiff;
          id = domain[i].question_id;
          rating = domain[i].question_rating;
        } else if (currDiff < diff) {
          diff = currDiff;
          id = domain[i].question_id;
          rating = domain[i].question_rating;
        }
      }
      return { id, rating, domain_code: domain[0].domain_code };
    });
    //create array of just the question ids and a hash map of the assesment question_ratings
    const justIds = questionIds.map((id) => id.id);
    let idRatingMap = new Map();
    questionIds.forEach((id) =>
      idRatingMap.set(id.id, { rating: id.rating, domain_code: id.domain_code })
    );
    //find all questions with ids from the justIds array
    let questions = await Question.findAll({
      where: { question_id: justIds },
      raw: true,
    });
    //replace the questoin rating with the assessment question rating
    questions = questions.map((question) => {
      let data = idRatingMap.get(question.question_id);
      question.question_rating = data.rating;
      question["domain_code"] = data.domain_code;
      return question;
    });
    return sendResponse(
      res,
      "success",
      200,
      "fetched first round of assessment questions",
      questions
    );
  } else {
    // already started assessment
    const alreadyCompletedQuestions = [];
    const alreadyAskedTags = [];
    answers.forEach((answer) => {
      alreadyCompletedQuestions.push(answer.question_id);
      alreadyAskedTags.push(answer.tag_id);
    });
    //create hash map to average out the tagscores for each domain
    const domainAverageMap = {};
    //create tag_ratings hash map
    const tagRatingMap = {};
    fetchRatings.forEach((rating) => {
      tagRatingMap[rating.tag_id] = {
        rating: rating.tag_rating,
        domain_code: rating["Tag.domain_code"],
      };
      if (!domainAverageMap[rating["Tag.domain_code"]]) {
        domainAverageMap[rating["Tag.domain_code"]] = {
          count: 1,
          sum: rating.tag_rating,
        };
      } else {
        let count = domainAverageMap[rating["Tag.domain_code"]].count;
        let sum = domainAverageMap[rating["Tag.domain_code"]].sum;
        domainAverageMap[rating["Tag.domain_code"]] = {
          count: count + 1,
          sum: sum + rating.tag_rating,
        };
      }
    });
    //average domain scores
    for (let key in domainAverageMap) {
      domainAverageMap[key].domain_average = Number.parseInt(
        (domainAverageMap[key].sum / domainAverageMap[key].count).toFixed(2)
      );
    }

    allIds.forEach((domain) => {
      for (let i = 0; i < domain.length; i++) {
        // if the domain already has a questions array
        if (
          domainAverageMap[domain[i].domain_code].hasOwnProperty("questions")
        ) {
          // check if the currents questions tag_id has already been covered
          if (
            domainAverageMap[domain[i].domain_code].questions[0].tag_id ===
            domain[i].tag_id
          ) {
            //if not add that question to the array
            domainAverageMap[domain[i].domain_code].questions.push(domain[i]);
          } else {
            //otherwise break out of this domain and go to the next
            break;
          }
          // if this domain doesnt already have a questions array and they havent
          //  already been asked a question with this tag_id
        } else if (alreadyAskedTags.indexOf(domain[i].tag_id) === -1) {
          //create questions array for this domain and add this question to it
          domainAverageMap[domain[i].domain_code]["questions"] = [domain[i]];
        }
        // if student has already been asked questions for all tags in this domain
        if (
          i === domain.length - 1 &&
          !domainAverageMap[domain[i].domain_code].hasOwnProperty("questions")
        ) {
          //loop over the questions for this domain again
          domain.forEach((question) => {
            // if the student hasnt already been asked this question add it to the questions array for this domain
            if (
              alreadyCompletedQuestions.indexOf(question.question_id) === -1
            ) {
              //if there is already a questions array, add this question to it otherwise create one
              domainAverageMap[domain[i].domain_code]["questions"]
                ? (domainAverageMap[domain[i].domain_code]["questions"] = [
                    ...domainAverageMap[domain[i].domain_code]["questions"],
                    question,
                  ])
                : (domainAverageMap[domain[i].domain_code]["questions"] = [
                    question,
                  ]);
            }
          });
        }
      }
    });

    //now pick the questions based on appropriate question rating
    let questionIds = [];
    let questionIdMap = new Map();
    for (let key in domainAverageMap) {
      let { domain_average } = domainAverageMap[key];
      let diff = null;
      let id, rating;
      domainAverageMap[key].questions.forEach((question) => {
        let currDiff;
        if (domain_average > question.question_rating) {
          currDiff = domain_average - question.question_rating;
        } else {
          currDiff = question.question_rating - domain_average;
        }

        if (diff === null) {
          diff = currDiff;
          id = question.question_id;
          rating = question.question_rating;
        } else if (currDiff < diff) {
          diff = currDiff;
          id = question.question_id;
          rating = question.question_rating;
        }
      });
      questionIds.push(id);
      questionIdMap.set(id, { rating, domain_code: key });
    }

    const fetchQuestions = await Question.findAll({
      where: { question_id: questionIds },
      raw: true,
    });

    const updatedQuestions = fetchQuestions.map((question) => {
      let data = questionIdMap.get(question.question_id);
      question.question_rating = data.rating;
      question["domain_code"] = data.domain_code;
      return question;
    });
    return sendResponse(
      res,
      "success",
      200,
      "fetched next round of assessment questions",
      updatedQuestions
    );
  }
};
*/
